##--------------------------------------------------------------------------------------------------------
## SCRIPT : Results from simulation w/o 3D copula prior
##
## Authors : Matthieu Authier, Nicolas Bousquet & Fanny Ouzoulias
## Last update : 2022-08-04
##
## R version 4.0.5 (2021-03-31) -- "Shake and Throw"
## Copyright (C) 2021 The R Foundation for Statistical Computing
## Platform: x86_64-w64-mingw32/x64 (64-bit)
##--------------------------------------------------------------------------------------------------------

lapply(c("tidyverse", "rstan"), 
       library, character.only = TRUE
       )

rm(list = ls())

source("stan/20220801_tlnorm_stan.r")
# n_sim <- 100
### get all simulation results
all_results <- sim_results(1, nom = "SimulSPMStan_Uni_r_") %>%
  mutate(prior = "Uniform")
gc()
all_results <- rbind(all_results,
                     sim_results(2, nom = "SimulSPMStan_Uni_r_") %>%
                       mutate(prior = "Uniform")
                     )
gc()
all_results <- rbind(all_results,
                     sim_results(3, nom = "SimulSPMStan_Uni_r_") %>%
                       mutate(prior = "Uniform")
                     )
gc()
# all_results <- rbind(all_results,
#                      sim_results(4, nom = "SimulSPMStan_Uni_r_") %>%
#                        mutate(prior = "Uniform")
#                      )
# gc()
# all_results <- rbind(all_results,
#                      sim_results(5, nom = "SimulSPMStan_Uni_r_") %>%
#                        mutate(prior = "Uniform")
#                      )
# gc()

all_results <- rbind(all_results,
                     sim_results(1, nom = "SimulSPMStan_Gam_r_") %>%
                       mutate(prior = "Gamma")
                     )
gc()
all_results <- rbind(all_results,
                     sim_results(2, nom = "SimulSPMStan_Gam_r_") %>%
                       mutate(prior = "Gamma")
                     )
gc()
all_results <- rbind(all_results,
                     sim_results(3, nom = "SimulSPMStan_Gam_r_") %>%
                       mutate(prior = "Gamma")
                     )
gc()
# all_results <- rbind(all_results,
#                      sim_results(4, nom = "SimulSPMStan_Gam_r_") %>%
#                        mutate(prior = "Gamma")
#                      )
# gc()
# all_results <- rbind(all_results,
#                      sim_results(5, nom = "SimulSPMStan_Gam_r_") %>%
#                        mutate(prior = "Gamma")
#                      )
# gc()


### add bias and coverage
K <- 5e+05
D0 <- 0.9 # initial condition
r_max <- 0.04 # taux de croissance intrinsèque
phi <- 0.005 # taux d'extraction
sigma_env <- 0.10 # stochasticité environnementale

all_results <- all_results %>%
  left_join(data.frame(param = c("r", "phi", "sigma", "D0", "K"),
                       truth = c(r_max, phi, sigma_env, D0, K)
                       ),
            by = "param"
            ) %>%
  mutate(bias = estimate - truth,
         coverage = ifelse(truth < lower, 0, 1)* ifelse(truth > upper, 0, 1)
         )

### save
write.table(all_results %>%
              mutate(n_survey = n_survey - 1),
            file = "output/20220804_SimulationsResults_IndependentPriors.txt",
            quote = FALSE, sep = "\t",
            row.names = FALSE, col.names = TRUE
            )
# 
# ### some plots
all_results %>%
  mutate(prior = factor(prior, levels = c("Uniform", "Gamma"))) %>%
  filter(param != "K") %>%
  ggplot(aes(x = n_survey, y = estimate, group = n_survey)) +
  geom_hline(data = data.frame(param = c("r", "phi", "sigma", "D0"),
                               y = c(r_max, phi, sigma_env, D0)
                               ),
             aes(yintercept = y), color = "tomato", size = 2
             ) +
  geom_boxplot() +
  facet_grid(param ~ prior, scales = "free_y") +
  scale_x_continuous(name = "Nb of abundance estimates",
                     breaks = 1:10,
                     labels = 1:10
                     ) +
  scale_y_sqrt() +
  theme_bw()

ggsave("output/ResultsSimulStan300_boxplot.png",
       units = "cm", dpi = 600, width = 15, height = 12
       )

### coverage
all_results %>%
  mutate(convergence = ifelse(rhat < 1.05, 1, 0),
         width = (upper - lower) / estimate,
         n_survey = n_survey - 1) %>%
  filter(param != "K",
         prior == "Uniform"
         ) %>%
  group_by(param, n_survey) %>%
  summarise(convergence = 100 * mean(convergence),
            bias = 100 * mean(bias / truth),
            coverage = 100 * mean(coverage),
            ci_width = 100 * mean(width)
            ) %>%
  pivot_longer(cols = c(convergence, bias, coverage, ci_width),
               names_to = "what",
               values_to = "value"
               ) %>%
  mutate(what = factor(what, levels = c("convergence", "bias", "coverage", "ci_width"))) %>%
  ggplot(aes(x = n_survey, y = value)) +
  geom_hline(data = data.frame(param = rep(c("r", "phi", "sigma", "D0"), each = 4),
                               what = rep(c("convergence", "bias", "coverage", "ci_width"), 4),
                               y = rep(c(100, 0, 80, NA), 4)
                               ) %>%
               mutate(what = factor(what, levels = c("convergence", "bias", "coverage", "ci_width", "K"))),
             aes(yintercept = y),
             color = "tomato", size = 2, linetype = "dotted"
             ) +
  geom_line() +
  facet_grid(what ~ param, scales = "free_y") +
  scale_x_continuous(name = "Nb of abundance estimates",
                     breaks = 1:10
                     ) +
  scale_y_continuous(name = "%") +
  theme_bw()

ggsave("output/ResultsSimulStan300_Uni_diagnostics.png",
       units = "cm", dpi = 600, width = 15, height = 12
       )

all_results %>%
  mutate(convergence = ifelse(rhat < 1.05, 1, 0),
         width = (upper - lower) / estimate,
         n_survey = n_survey - 1) %>%
  filter(param != "K",
         prior != "Uniform"
         ) %>%
  group_by(param, n_survey) %>%
  summarise(convergence = 100 * mean(convergence),
            bias = 100 * mean(bias / truth),
            coverage = 100 * mean(coverage),
            ci_width = 100 * mean(width)
            ) %>%
  pivot_longer(cols = c(convergence, bias, coverage, ci_width),
               names_to = "what",
               values_to = "value"
               ) %>%
  mutate(what = factor(what, levels = c("convergence", "bias", "coverage", "ci_width"))) %>%
  ggplot(aes(x = n_survey, y = value)) +
  geom_hline(data = data.frame(param = rep(c("r", "phi", "sigma", "D0"), each = 4),
                               what = rep(c("convergence", "bias", "coverage", "ci_width"), 4),
                               y = rep(c(100, 0, 80, NA), 4)
                               ) %>%
    mutate(what = factor(what, levels = c("convergence", "bias", "coverage", "ci_width", "K"))),
  aes(yintercept = y),
  color = "tomato", size = 2, linetype = "dotted"
  ) +
  geom_line() +
  facet_grid(what ~ param, scales = "free_y") +
  scale_x_continuous(name = "Nb of abundance estimates",
                     breaks = 1:10
                     ) +
  scale_y_continuous(name = "%") +
  theme_bw()

ggsave("output/ResultsSimulStan300_Gam_diagnostics.png",
       units = "cm", dpi = 600, width = 15, height = 12
       )
