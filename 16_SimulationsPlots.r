##--------------------------------------------------------------------------------------------------------
## SCRIPT : Simulated trajectories
##
## Authors : Matthieu Authier, Nicolas Bousquet & Fanny Ouzoulias
## Last update : 2022-08-05
##
## R version 4.0.5 (2021-03-31) -- "Shake and Throw"
## Copyright (C) 2021 The R Foundation for Statistical Computing
## Platform: x86_64-w64-mingw32/x64 (64-bit)
##--------------------------------------------------------------------------------------------------------

lapply(c("tidyverse", "rstan"), 
       library, character.only = TRUE
       )

rm(list = ls())

K <- 5e5
### stationary case
load("output/SimulSPMStan_Uni_r_1.RData")
rm(all_data_fit)

all_simul <- do.call('rbind', all_simul_data) %>%
  mutate(simul = rep(1:100, each = 300)) %>%
  filter(param != "Bt.obs") %>%
  mutate(value = ifelse(param == "Bt", value / K, value),
         param = ifelse(param == "Bt", "Depletion", "(By-)Catch"),
         param = factor(param, levels = c("Depletion", "(By-)Catch"))
         )

all_simul %>%
  ggplot(aes(x = time, y = value, group = simul)) +
  geom_line(alpha = 0.1, color = "midnightblue") +
  facet_wrap(~ param, ncol = 1, scales = "free_y") +
  scale_y_sqrt(name = "") +
  theme_bw()

ggsave("output/Simul_stationary.png",
       units = "cm", dpi = 600, width = 15, height = 12
       )
rm(all_simul_data, all_simul)
gc()

### increasing case
load("output/SimulSPMStan_Mono_Gam_r_1.RData")
rm(all_data_fit)

all_simul <- do.call('rbind', all_simul_data) %>%
  mutate(simul = rep(1:100, each = 90)) %>%
  filter(param != "Bt.obs") %>%
  mutate(value = ifelse(param == "Bt", value / K, value),
         param = ifelse(param == "Bt", "Depletion", "(By-)Catch"),
         param = factor(param, levels = c("Depletion", "(By-)Catch"))
         )

all_simul %>%
  ggplot(aes(x = time, y = value, group = simul)) +
  geom_line(alpha = 0.1, color = "midnightblue") +
  facet_wrap(~ param, ncol = 1, scales = "free_y") +
  scale_y_sqrt(name = "") +
  theme_bw()

ggsave("output/Simul_increasing.png",
       units = "cm", dpi = 600, width = 15, height = 12
       )
rm(all_simul_data, all_simul)
gc()

### decreasing case
load("output/SimulSPMStan_Mismatch_Gam_r_1.RData")
rm(all_data_fit)

all_simul <- do.call('rbind', all_simul_data) %>%
  mutate(simul = rep(1:100, each = 90)) %>%
  filter(param != "Bt.obs") %>%
  mutate(value = ifelse(param == "Bt", value / K, value),
         param = ifelse(param == "Bt", "Depletion", "(By-)Catch"),
         param = factor(param, levels = c("Depletion", "(By-)Catch"))
         )

all_simul %>%
  ggplot(aes(x = time, y = value, group = simul)) +
  geom_line(alpha = 0.1, color = "midnightblue") +
  facet_wrap(~ param, ncol = 1, scales = "free_y") +
  scale_y_sqrt(name = "") +
  theme_bw()

ggsave("output/Simul_decreasing.png",
       units = "cm", dpi = 600, width = 15, height = 12
       )
rm(all_simul_data, all_simul)
gc()