#==============================================================================#
# Functions to produce a copula representation of the joint prior on (sigma,r,phi)
#===============================================================================#

# COPULES
library(copula)  
library(fgac)
library(ismev)
library(mclust)  
#library(FDGcopulas)
library(VineCopula)

#data <- read.csv2("results_extinction_monotonie_new_spm.csv",sep="\t")
data <- read.csv2("calib_phi_sigma_r.csv",sep="\t")

sigma <- as.numeric(data[,1])
r <- as.numeric(data[,2])
phi <- as.numeric(data[,3])

index <- as.numeric(data[,4])==1

data.num <- data.frame(sigma,r,phi,as.numeric(data[,4]))
colnames(data.num) <- c("sigma","r","phi","plausibility")

pairs(data.num[1:3], main = "",
      pch = 21, bg = c("red", "blue")[unclass(data.num$plausibility)])




sigma.ok <- sigma[index]
r.ok <- r[index]
phi.ok <- phi[index]

# uniform by ranking
U1 <- rank(sigma.ok)/(length(sigma.ok)+1)
U2 <- rank(r.ok)/(length(r.ok)+1)
U3 <- rank(phi.ok)/(length(phi.ok)+1)
sample <- cbind(U1,U2,U3)
colnames(sample) <- c("sigma","r","phi")
# scatterplot on the uniform space
pairs(sample)


# trivariate copula estimation and testing

test.vines <- RVineStructureSelect(sample,familyset=NA,indeptest=T)
vine.matrix <- test.vines$Matrix
select.vine <- RVineCopSelect(sample,familyset=NA,vine.matrix,indeptest=T)
print("copula selected = ")
print(select.vine)

# Copula simulation
U.sim <- RVineSim(dim(sample)[1],select.vine)
quartz()
pairs(U.sim,pch=4)


