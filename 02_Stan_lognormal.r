##--------------------------------------------------------------------------------------------------------
## SCRIPT : Lognormal model
##
## Authors : Matthieu Authier, Nicolas Bousquet & Fanny Ouzoulias
## Last update : 2022-06-23
##
## R version 4.0.5 (2021-03-31) -- "Shake and Throw"
## Copyright (C) 2021 The R Foundation for Statistical Computing
## Platform: x86_64-w64-mingw32/x64 (64-bit)
##--------------------------------------------------------------------------------------------------------

lapply(c("tidyverse", "lubridate", "rstan", "loo"), 
       library, character.only = TRUE
       )

rm(list = ls())

source("R/functions/20220623_tlnorm.R")   # fonctions pour mettre en place le Gibbs

set.seed(20220622)
### length of time series
n_obs <- 100 # nb d'années de captures 
### measurement error
cv <- 0.2
### carrying capacity
K <- 5e+05
theta0 <- 0.9 # initial condition
r_max <- 0.04 # taux de croissance intrinsèque
phi <- 0.005 # taux d'extraction
gamma_pt <- 2.4 # paramètre SPM
q_buoy <- 1.0 # fraction échouages bycatch
sigma_env <- 0.25 # stochasticité environnementale

simul_data <- spm_sampling(n_obs = n_obs,
                           theta0 = theta0, 
                           r_max = r_max, 
                           phi = phi,
                           sigma_env = sigma_env,
                           gamma_pt = gamma_pt,
                           q_buoy = q_buoy,
                           cv = cv, 
                           B0 = K * theta0
                           ) %>%
  do.call('cbind', .) %>%
  as.data.frame() %>%
  mutate(time = 1:n()) %>%
  pivot_longer(cols = -time,
               names_to = "param",
               values_to = "value"
               )

simul_data %>%
  ggplot(aes(x = time, y = value, group = param, color = param)) +
  geom_line() +
  scale_y_log10() +
  theme_bw()

### Stan
rstan_options(auto_write = TRUE)
options(mc.cores = parallel::detectCores())
spm <- stan_model(file = "stan/Kspm_unscaledprior.stan", 
                  model_name = "Stochastic MSY with prior on theta0"
                  )

stan_data <- list(n_strandings = n_obs,
                  n_surveys = length(c(1, seq(10, 100, 10))),
                  STRANDINGS = simul_data %>%
                    filter(param == "Ce") %>%
                    pull(value),
                  SURVEY = simul_data %>%
                    filter(time %in% c(1, seq(10, 100, 10))) %>%
                    pivot_wider(names_from = param,
                                values_from = value
                                ) %>%
                    select("Ce", "Bt.obs") %>%
                    mutate(cv = cv) %>%
                    as.matrix(),
                  gamma = gamma_pt,
                  B0 = simul_data %>%
                    filter(time == 1,
                           param == "Bt.obs"
                           ) %>%
                    pull(value),
                  q = 1.0,
                  upper_bound_r = 0.1,
                  upper_bound_phi = 0.1,
                  lower_bound_theta0 = 0.1
                  )

### test
fit = sampling(spm, 
               data = stan_data, 
               iter = 700, 
               warmup = 200, 
               thin = 1, 
               chains = 4, 
               # control = list(max_treedepth = 15, adapt_delta = 0.95),
               pars = c("r", "phi", "sigma", "theta0")
               )

stan_rhat(fit)

plot(fit,
     plotfun = "stan_trace",
     pars = c("r", "phi", "sigma", "theta0"),
     inc_warmup = TRUE
     )

print(fit, digits = 4)

plot(fit,
     plotfun = "stan_hist",
     pars = c("r", "phi", "sigma", "theta0"),
     inc_warmup = FALSE
     )

plot(fit,
     plotfun = "stan_dens",
     pars = c("r", "phi", "sigma", "theta0"),
     inc_warmup = FALSE
     )

pairs(fit)

### extract posterior, e.g. r
posterior_r <- rstan::extract(fit, 'r')$r
hist(posterior_r)

### because theta0 is underestimated, K is overestimated
K <- stan_data$B0 / rstan::extract(fit, 'theta0')$theta0
quantile(K, probs = c(0.1, 0.5, 0.9))
