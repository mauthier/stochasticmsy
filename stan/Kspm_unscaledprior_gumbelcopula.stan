functions {
  // Gumbel copula from https://spinkney.github.io/helpful_stan_functions/group__gumbel.html
  real gumbel_copula(real u, real v, real theta) {
    real neg_log_u = -log(u);
    real log_neg_log_u = log(neg_log_u);
    real neg_log_v = -log(v);
    real log_neg_log_v = log(neg_log_v);
    real log_temp = log_sum_exp(theta * log_neg_log_u, theta * log_neg_log_v);
    real theta_m1 = theta - 1;
    if (theta < 1) {
      reject("theta must be >= 1");
    }
    if (is_inf(theta)) {
      if (u == v) {
        return 0;
      }
      else{
        return negative_infinity();
      }
    }
  
    return theta_m1 * log_neg_log_u + theta_m1 * log_neg_log_v
     + neg_log_u + neg_log_v - exp(log_temp / theta)
     + log_sum_exp(2 * theta_m1 / -theta * log_temp, log(theta_m1) + (1 - 2 * theta) / theta * log_temp);
  }

  real upper_bound_sigma(real phi, real r, real gamma) {
    // upper bound for sigma given phi, r and gamma
    // phi: extraction rate	
    // r: growth rate
    // gamma: shape of the Pella-Tomlinson DD function
    real value;
    // sanity checks
    if (!(phi >= 0)) {
     reject("upper_bound_sigma(phi, r, gamma): phi must be positive; found phi = ", phi);
    }
    if (!(r >= 0)) {
     reject("upper_bound_sigma(phi, r, gamma): r must be positive; found r = ", r);
    }
    if (!(gamma >= 0)) {
     reject("upper_bound_sigma(phi, r, gamma): gamma must be positive; found gamma = ", gamma);
    }
    value = (1 + inv(gamma)) * log1p(gamma); // numerator
    value += -log(gamma) - log1m(phi + r * (gamma + 1) * inv(gamma)); // denominator
    return sqrt(expm1(value));
  }

  // for Beta likelihood on strandings
  real D_theta_log(real phi, real r, real gamma, real q, real theta0, real B0) {
    // logarithm of D(theta)
    // phi: extraction rate	
    // r: growth rate
    // gamma: shape of the Pella-Tomlinson DD function
    // q: buoyancy probability
    // theta0: initial depletion
    // B0: initial abundance/biomass
    real value;
    // sanity checks
    if (!(phi >= 0)) {
     reject("D_theta_log(phi, r, gamma, q, theta0, B0): phi must be positive; found phi = ", phi);
    }
    if (!(r >= 0)) {
     reject("D_theta_log(phi, r, gamma, q, theta0, B0): r must be positive; found r = ", r);
    }
    if (!(gamma >= 0)) {
     reject("D_theta_log(phi, r, gamma, q, theta0, B0): gamma must be positive; found gamma = ", gamma);
    }
    if (!(q >= 0)) {
     reject("D_theta_log(phi, r, gamma, q, theta0, B0): q must be positive; found q = ", q);
    }
    if (!(q <= 1)) {
     reject("D_theta_log(phi, r, gamma, q, theta0, B0): q must be less than or equal to one; found q = ", q);
    }
    if (!(theta0 >= 0)) {
     reject("D_theta_log(phi, r, gamma, q, theta0, B0): theta0 must be positive; found theta0 = ", theta0);
    }
    if (!(B0 >= 0)) {
     reject("D_theta_log(phi, r, gamma, sigma, q, theta0, B0): B0 must be less than or equal to one; found B0 = ", B0);
    }
    value = log(B0) - log(theta0) - log(q) - log(phi);
    value += inv(gamma) * (log(r * (gamma + 1)) - log(gamma * (1 - phi + r) + r));
	return value;
  }

  real strandings_lpdf(real[] y, real phi, real r, real gamma, real sigma, real q, real theta0, real B0) {
    // log likelihood for strandings
    // y: data
    // phi: extraction rate 
    // r: growth rate
    // gamma: shape of the Pella-Tomlinson DD function
    // sigma: environmental stochasticity
    // q: buoyancy probability
    // theta0: initial depletion
    // B0: initial abundance/biomass
    int n = size(y); // length of time series for y
    real coef;
    real value;
    // sanity checks
    if (!(n >= 2)) {
     reject("strandings_lpdf(y, phi, r, gamma, sigma, q, theta0, B0): y must be of size greater than one; found size = ", n);
    }
    if (!(phi >= 0)) {
     reject("strandings_lpdf(y, phi, r, gamma, sigma, q, theta0, B0): phi must be positive; found phi = ", phi);
    }
    if (!(r >= 0)) {
     reject("strandings_lpdf(y, phi, r, gamma, sigma, q, theta0, B0): r must be positive; found r = ", r);
    }
    if (!(gamma >= 0)) {
     reject("strandings_lpdf(y, phi, r, gamma, sigma, q, theta0, B0): gamma must be positive; found gamma = ", gamma);
    }
    if (!(sigma >= 0)) {
     reject("strandings_lpdf(y, phi, r, gamma, sigma, q, theta0, B0): sigma must be positive; found sigma = ", sigma);
    }
    if (!(q >= 0)) {
     reject("strandings_lpdf(y, phi, r, gamma, sigma, q, theta0, B0): q must be positive; found q = ", q);
    }
    if (!(q <= 1)) {
     reject("strandings_lpdf(y, phi, r, gamma, sigma, q, theta0, B0): q must be less than or equal to one; found q = ", q);
    }
    if (!(theta0 >= 0)) {
     reject("strandings_lpdf(y, phi, r, gamma, sigma, q, theta0, B0): theta0 must be positive; found theta0 = ", theta0);
    }
    if (!(B0 >= 0)) {
     reject("strandings_lpdf(y, phi, r, gamma, sigma, q, theta0, B0): B0 must be less than or equal to one; found B0 = ", B0);
    }
    coef = exp(log(theta0) - log(q) - log(phi) - log(B0));
    value = 0.0;
    // currently does not include the first datum
    for(t in 2:n) {
      real mu = log(y[t-1] + ((gamma + 1) * r / gamma) * y[t-1] * (1 - pow(coef * y[t-1], gamma)) - phi * y[t-1]) - 0.5 * square(sigma);
      value += lognormal_lpdf(y[t] | mu, sigma);
    }
    return value;
  }

  real biomass_lpdf(real[] y, real phi, real q) {
    // log likelihood for abundance/biomass given strandings
    // y: vector of length 3, with 1-strandings, 2-estimated abundance/biomass and 3-aasociated coefficient of variation
    // phi: extraction rate
    // q: buoyancy probability
    int n = size(y); // length of time series for y
    real tau; // scale parameter of log-normal distribution
    real mu;  // location parameter of log-normal distribution
    
    // sanity checks
    if (!(n == 3)) {
     reject("biomass_lpdf(y, phi, q): y must be of size 3; found size = ", n);
    }
    if (!(phi >= 0)) {
     reject("biomass_lpdf(y, phi, q): phi must be positive; found phi = ", phi);
    }
    if (!(q >= 0)) {
     reject("biomass_lpdf(y, phi, q): q must be positive; found q = ", q);
    }
    if (!(q <= 1)) {
     reject("biomass_lpdf(y, phi, q): q must be less than or equal to one; found q = ", q);
    }
    tau = sqrt(log1p(y[3] * y[3]));
    mu = log(y[1]) - log(q) - log(phi) - 0.5 * square(tau);
    return lognormal_lpdf(y[2] | mu, tau);
  }
}

data {
  int<lower = 2> n_strandings;
  int<lower = 1> n_surveys;
  real<lower = 0.0> STRANDINGS[n_strandings]; // strandings time series
  real<lower = 0.0> SURVEY[n_surveys, 3];     // abundance/biomass
  real<lower = 0.0> gamma;         // shape of the Pella-Tomlinson DD function
  real<lower = 0.0> B0;            // B0: initial abundance/biomass
  real<lower = 0.0, upper = 1.0> q;// buoyancy probability
  real<lower = 0.0> upper_bound_r; // upper bound for r
  real<lower = 0.0> upper_bound_phi; // upper bound for phi
  real<lower = 0.0> lower_bound_theta0; // lower bound for theta0
  real<lower = 0.0, upper = 1.0> kendalls_tau;
}
//
//transformed data {
//	
//}

parameters {
  real<lower = 0.0, upper = 1.0> unscaled_r;    // growth rate
  real<lower = 0.0, upper = 1.0> unscaled_phi;  // extraction rate
  real<lower = 0.0, upper = 1.0> unscaled_sigma;// environmental stochasticity
  real<lower = lower_bound_theta0, upper = 1.0> theta0; // theta0: initial depletion
}

transformed parameters {
  real r = unscaled_r * upper_bound_r;    // growth rate
  real phi = unscaled_phi * upper_bound_phi;  // extraction rate
  real sigma_max = upper_bound_sigma(phi, r, gamma);
  real sigma = unscaled_sigma * sigma_max; // environmental stochasticity
  real theta_copula = inv(1 - kendalls_tau);
}

model {
  // priors
  unscaled_r ~ uniform(0.0, 1.0);
  unscaled_phi ~ uniform(0.0, 1.0);
  unscaled_sigma ~ uniform(0.0, 1.0);
  target += gumbel_copula(unscaled_r, unscaled_phi, theta_copula);
  // likelihood
  // strandings
  target += strandings_lpdf(STRANDINGS| phi, r, gamma, sigma, q, theta0, B0);
  // surveys
  for(i in 1:n_surveys) {
  	target += biomass_lpdf(SURVEY[i, ]| phi, q);
  }
}

generated quantities {
  real K = B0 * inv(theta0);
  //real<lower = 0.0> check = gamma_rng(3.645, 91.40768);
}
