#====================================#
# The Truncated Normal Distribution
#====================================#

# density
dtnorm <- function(x, location = 0, scale = 1, left = -1e6, right = 1e6) {
  out = dnorm(x, location, scale) / (pnorm(right, location, scale) - pnorm(left, location, scale))
  out[(x < left) | (x > right)] = 0
  return(out)
}

# distribution function
ptnorm <- function(q, location = 0, scale = 1, left = -1e6, right = 1e6) {
  out = (pnorm(q, location, scale) - pnorm(left, location, scale)) /
    (pnorm(right, location, scale) - pnorm(left, location, scale))
  out[q < left] = 0
  out[q > right] = 1
  return(out)
}

# quantile function
qtnorm <- function(p, location = 0, scale = 1, left = -1e6, right = 1e6) {
  out <- qnorm((1 - p) * pnorm(left, location, scale) + p * pnorm(right, location, scale),
               location, scale
               )
  return(out)
}

# random generation
rtnorm <- function(n, location = 0, scale = 1, left = -1e6, right = 1e6) {
  out <- qtnorm(runif(n), location, scale, left, right)
  return(out)
}

#=======================================
# The Truncated Lognormal Distribution
#=======================================

# density
dtlnorm <- function(x, location = 0, scale = 1, left = -1e6, right = 1e6) {
  out = dlnorm(x, meanlog = location, sdlog = scale) / (plnorm(right, meanlog = location, sdlog = scale) - plnorm(left, meanlog = location, sdlog = scale))
  out[(x < left) | (x > right)] = 0
  return(out)
}

# distribution function
ptlnorm <- function(q, location = 0, scale = 1, left = -1e6, right = 1e6) {
  out = (plnorm(q, meanlog = location, sdlog = scale) - plnorm(left, meanlog = location, sdlog = scale)) /
    (plnorm(right, meanlog = location, sdlog = scale) - plnorm(left, meanlog = location, sdlog = scale))
  out[q < left] = 0
  out[q > right] = 1
  return(out)
}

# quantile function
qtlnorm <- function(p, location = 0, scale = 1, left = -1e6, right = 1e6) {
  out <- qlnorm((1 - p) * plnorm(left, meanlog = location, sdlog = scale) + p * plnorm(right, meanlog = location, sdlog = scale),
                meanlog = location, sdlog = scale
                )
  return(out)
}

# random generation
rtlnorm <- function(n, location = 0, scale = 1, left = -1e6, right = 1e6) {
  out <- qtlnorm(runif(n), location, scale, left, right)
  return(out)
}

#====================================#
# LOGKLIKELIHOOD OF STRANDINGS
#====================================#

log_lik_strandings <- function(y, 
                               q_buoy, 
                               phi, 
                               r_max,
                               D0, 
                               gamma_pt, 
                               sigma_env,
                               B0
                               ) {
  mu <- exp(log(D0) - log(q_buoy) - log(phi) - log(B0))
  lpdf <- rep(0, length(y))
  # no model for the initial condition
  for (t in c(2:length(y))) {
    g_prov <- log(y[t-1] + 
                    ((gamma_pt+1) * r_max / gamma_pt) * y[t - 1] * (1 - (mu * y[t - 1])^gamma_pt) - phi * y[t-1]
                  ) - 0.5 * sigma_env * sigma_env
    
    lpdf[t] <- log(dtlnorm(y[t], location = g_prov, scale = sigma_env, left = 0))
  }
  lpdf[is.na(lpdf)] <- -Inf
  out <- sum(lpdf)
  return(lpdf)
}

#====================================#
# Data Simulation Model
#====================================#

spm_sampling <- function(n_obs,
                         D0, 
                         r_max, 
                         phi,
                         sigma_env,
                         gamma_pt,
                         q_buoy,
                         cv,
                         B0,
                         kendall_tau = NULL
                         ) {
  ### check
  if(length(q_buoy) %in% c(1, n_obs) && all(!is.na(q_buoy))) {
    if(length(q_buoy) == 1) {
      q_buoy <- rep(q_buoy, n_obs)
    }
  } else {
    stop("Parameter 'q_buoy' must be of length 1 or 'n_obs' with no NA")
  }
  ### parameter transformations
  tau <- sqrt(log1p(cv * cv))
  mu <- exp(log(D0) - log(q_buoy) - log(phi) - log(B0))
  out <- NULL
  strandings <- abundance <- rep(NA, n_obs)
  # initial condition
  abundance[1] <- B0
  strandings[1] <- round(q_buoy[1] * phi * B0)
  # process model
  for (t in 2:n_obs) {
    g_prov <- log(strandings[t-1] + 
                    ((gamma_pt+1) * r_max / gamma_pt) * strandings[t-1] * (1 - (mu[t] * strandings[t-1])^gamma_pt) - phi * strandings[t-1]
                  ) - 0.5 * sigma_env * sigma_env
    strandings[t] <- round(rtlnorm(1, location = g_prov, scale = sigma_env, left = 0))
    abundance[t] <- strandings[t] / (phi * q_buoy[t])	
  }
  ### observation model 
  obs_abundance <- round(rlnorm(length(abundance), log(abundance) -0.5 * tau * tau, tau))
  
  # Verification de l'existence de la log-vraisemblance optimale des strandings
  check_likelihood <- log_lik_strandings(y = strandings,
                                         q_buoy = mean(q_buoy),
                                         phi = phi,
                                         r_max = r_max,
                                         D0 = D0,
                                         gamma_pt = gamma_pt,
                                         sigma_env = sigma_env,
                                         B0 = obs_abundance[1] # use the first observation here
                                         )
  
  ### check kendall's tau
  if(!is.null(kendall_tau)) {
    prop <- cor(1:n_obs, strandings, method = "kendall")
    while(prop < kendall_tau || is.infinite(check_likelihood) || is.na(check_likelihood)) {
      # process model
      for (t in 2:n_obs) {
        g_prov <- log(strandings[t-1] + 
                        ((gamma_pt+1) * r_max / gamma_pt) * strandings[t-1] * (1 - (mu[t] * strandings[t-1])^gamma_pt) - phi * strandings[t-1]
                      ) - 0.5 * sigma_env * sigma_env
        strandings[t] <- round(rtlnorm(1, location = g_prov, scale = sigma_env, left = 0))
        abundance[t] <- strandings[t] / (phi * q_buoy[t])	
      }
      ### observation model 
      obs_abundance <- round(rlnorm(length(abundance), log(abundance) -0.5 * tau * tau, tau))
      
      prop <- cor(1:n_obs, strandings, method = "kendall")
      check_likelihood <- log_lik_strandings(y = strandings,
                                             q_buoy = mean(q_buoy),
                                             phi = phi,
                                             r_max = r_max,
                                             D0 = D0,
                                             gamma_pt = gamma_pt,
                                             sigma_env = sigma_env,
                                             B0 = obs_abundance[1] # use the first observation here
                                             )
    }
  } else {
    while (is.infinite(check_likelihood) || is.na(check_likelihood)) {
      # process model
      for (t in 2:n_obs) {
        g_prov <- log(strandings[t-1] + 
                        ((gamma_pt+1) * r_max / gamma_pt) * strandings[t-1] * (1 - (mu[t] * strandings[t-1])^gamma_pt) - phi * strandings[t-1]
                      ) - 0.5 * sigma_env * sigma_env
        strandings[t] <- round(rtlnorm(1, location = g_prov, scale = sigma_env, left = 0))
        abundance[t] <- strandings[t] / (phi * q_buoy[t])	
      }
      ### observation model 
      obs_abundance <- round(rlnorm(length(abundance), log(abundance) -0.5 * tau * tau, tau))
      
      check_likelihood <- log_lik_strandings(y = strandings,
                                             q_buoy = mean(q_buoy),
                                             phi = phi,
                                             r_max = r_max,
                                             D0 = D0,
                                             gamma_pt = gamma_pt,
                                             sigma_env = sigma_env,
                                             B0 = obs_abundance[1] # use the first observation here
                                             )
    }
  }

  out <- list("Ce" = strandings,
              "Bt" = round(abundance),
              "Bt.obs" = obs_abundance
              )
  
  return(out)
} 

pella_tomlinson <- function(n_obs,
                            D0, 
                            r_max, 
                            phi,
                            sigma_env,
                            gamma_pt,
                            q_buoy,
                            assumed_q = NULL,
                            cv,
                            K,
                            kendall_tau = NULL
                            ) {
  ### check
  if(length(q_buoy) %in% c(1, n_obs) && all(!is.na(q_buoy))) {
    if(length(q_buoy) == 1) {
      q_buoy <- rep(q_buoy, n_obs)
    }
  } else {
    stop("Parameter 'q_buoy' must be of length 1 or 'n_obs' with no NA")
  }
  ### parameter transformations
  strandings <- abundance <- rep(NA, n_obs)
  ## environmental stochasticity
  epsilon <- rlnorm(n_obs - 1, meanlog = -0.5 * sigma_env * sigma_env, sdlog = sigma_env)
  ## initial condition
  abundance[1] <- ceiling(K * D0)
  strandings[1] <- abundance[1] * phi * q_buoy[1]
  for(t in 2:n_obs) {
    # pella-tomlinson dynamics
    abundance[t] <- abundance[t - 1] * (1 + (gamma_pt + 1) / gamma_pt * r_max * (1 - (abundance[t - 1] / K)^gamma_pt) - phi * q_buoy[t - 1]) * epsilon[t - 1]
    # use integers
    abundance[t] <- max(0, floor(abundance[t]))
    strandings[t] <- abundance[t] * phi * q_buoy[t]
  }
  
  ### observation model
  tau <- sqrt(log1p(cv * cv))
  obs_abundance <- round(rlnorm(length(abundance), log(abundance) -0.5 * tau * tau, tau))
  
  # Verification de l'existence de la log-vraisemblance optimale des strandings
  check_likelihood <- log_lik_strandings(y = strandings,
                                         q_buoy = ifelse(is.null(assumed_q), mean(q_buoy), assumed_q),
                                         phi = phi,
                                         r_max = r_max,
                                         D0 = D0,
                                         gamma_pt = gamma_pt,
                                         sigma_env = sigma_env,
                                         B0 = obs_abundance[1] # use the first observation here
                                         )
  
  ### check kendall's tau
  if(!is.null(kendall_tau)) {
    prop <- cor(1:n_obs, strandings, method = "kendall")
    while(prop < kendall_tau || is.infinite(check_likelihood) || is.na(check_likelihood)) {
      ## environmental stochasticity
      epsilon <- rlnorm(n_obs - 1, meanlog = -0.5 * sigma_env * sigma_env, sdlog = sigma_env)
      # process model
      for (t in 2:n_obs) {
        abundance[t] <- abundance[t - 1] * (1 + (gamma_pt + 1) / gamma_pt * r_max * (1 - (abundance[t - 1] / K)^gamma_pt) - phi * q_buoy[t - 1]) * epsilon[t - 1]
        abundance[t] <- max(0, floor(abundance[t]))
        strandings[t] <- abundance[t] * phi * q_buoy[t]
      }
      ### observation model 
      obs_abundance <- round(rlnorm(length(abundance), log(abundance) -0.5 * tau * tau, tau))
      
      prop <- cor(1:n_obs, strandings, method = "kendall")
      check_likelihood <- log_lik_strandings(y = strandings,
                                             q_buoy = ifelse(is.null(assumed_q), mean(q_buoy), assumed_q),
                                             phi = phi,
                                             r_max = r_max,
                                             D0 = D0,
                                             gamma_pt = gamma_pt,
                                             sigma_env = sigma_env,
                                             B0 = obs_abundance[1] # use the first observation here
                                             )
    }
  } else {
    while (is.infinite(check_likelihood) || is.na(check_likelihood)) {
      ## environmental stochasticity
      epsilon <- rlnorm(n_obs - 1, meanlog = -0.5 * sigma_env * sigma_env, sdlog = sigma_env)
      # process model
      for (t in 2:n_obs) {
        abundance[t] <- abundance[t - 1] * (1 + (gamma_pt + 1) / gamma_pt * r_max * (1 - (abundance[t - 1] / K)^gamma_pt) - phi * q_buoy[t - 1]) * epsilon[t - 1]
        abundance[t] <- max(0, floor(abundance[t]))
        strandings[t] <- abundance[t] * phi * q_buoy[t]
      }
      ### observation model 
      obs_abundance <- round(rlnorm(length(abundance), log(abundance) -0.5 * tau * tau, tau))
      
      check_likelihood <- log_lik_strandings(y = strandings,
                                             q_buoy = ifelse(is.null(assumed_q), mean(q_buoy), assumed_q),
                                             phi = phi,
                                             r_max = r_max,
                                             D0 = D0,
                                             gamma_pt = gamma_pt,
                                             sigma_env = sigma_env,
                                             B0 = obs_abundance[1] # use the first observation here
                                             )
    }
  }
  
  out <- list("Ce" = round(strandings),
              "Bt" = abundance,
              "Bt.obs" = obs_abundance
              )
  
  return(out)
}

spm_wrapper <- function() {
  spm_sampling(n_obs = n_obs,
               D0 = D0, 
               r_max = r_max, 
               phi = phi,
               sigma_env = sigma_env,
               gamma_pt = gamma_pt,
               q_buoy = q_buoy,
               cv = cv, 
               B0 = K * D0,
               kendall_tau = kendall_tau
               ) %>%
    do.call('cbind', .) %>%
    as.data.frame() %>%
    mutate(time = 1:n()) %>%
    pivot_longer(cols = -time,
                 names_to = "param",
                 values_to = "value"
                 ) %>%
    as.data.frame()
}

pt_wrapper <- function(q_buoy) {
  pella_tomlinson(n_obs = n_obs,
                  D0 = D0, 
                  r_max = r_max, 
                  phi = phi,
                  sigma_env = sigma_env,
                  gamma_pt = gamma_pt,
                  q_buoy = q_buoy,
                  assumed_q = assumed_q,
                  cv = cv, 
                  K = K,
                  kendall_tau = kendall_tau
                  ) %>%
    do.call('cbind', .) %>%
    as.data.frame() %>%
    mutate(time = 1:n()) %>%
    pivot_longer(cols = -time,
                 names_to = "param",
                 values_to = "value"
                 ) %>%
    as.data.frame()
}

#====================================#
# Stan related functions
#====================================#

rstan_options(auto_write = TRUE)
options(mc.cores = parallel::detectCores())

stan_data <- function(simul_data, 
                      survey_time, 
                      copula = TRUE
                      ) {
  simul_data <- simul_data %>%
    filter(time <= max(survey_time))
  out <- list(n_strandings = max(survey_time),
              n_surveys = length(survey_time),
              STRANDINGS = simul_data %>%
                filter(param == "Ce") %>%
                pull(value),
              SURVEY = simul_data %>%
                filter(time %in% survey_time) %>%
                pivot_wider(names_from = param,
                            values_from = value
                            ) %>%
                select("Ce", "Bt.obs") %>%
                mutate(cv = cv) %>%
                as.matrix(),
              gamma = gamma_pt,
              B0 = simul_data %>%
                filter(time %in% survey_time,
                       param == "Bt.obs") %>%
                pull(value) %>%
                first(),
              # B0 = simul_data %>%
              #   filter(time == 1,
              #          param == "Bt.obs"
              #          ) %>%
              #   pull(value),
              q = 1.0,
              upper_bound_phi = 0.1,
              upper_bound_r = 0.1,
              upper_bound4sigma = 2.0,
              lower_bound_D0 = 0.1,
              theta_RG = -1.21,
              theta_G = 0.39,
              theta_RT = c(2.97, 0.18),
              COPULA = ifelse(copula, 1, 0)
              )
  return(out)
}

fitWstan <- function(compiled_model, 
                     simul_data, 
                     survey_freq,
                     copula = TRUE
                     ) {
  # all_survey_time <- c(1,
  #                      seq(from = survey_freq, 
  #                          to = simul_data %>%
  #                            pull(time) %>%
  #                            max(), 
  #                          by = survey_freq
  #                          )
  #                      )
  # do not assume there's a survey available here
  all_survey_time <- seq(from = survey_freq, 
                         to = simul_data %>%
                           pull(time) %>%
                           max(), 
                         by = survey_freq
                         )
  foo <- function(i) {
    all_survey_time[1:i]
  }
  n_survey_time <- map(.x = 1:length(all_survey_time),
                       .f = foo
                       )
  all_stan_data <- lapply(n_survey_time,
                          stan_data,
                          simul_data = simul_data,
                          copula = copula
                          )
  
  all_fit <- lapply(all_stan_data,
                    function(datalist) {
                      fit = sampling(compiled_model, 
                                     data = datalist, 
                                     iter = 1000, 
                                     warmup = 500, 
                                     thin = 1, 
                                     chains = 4, 
                                     control = list(max_treedepth = 15, adapt_delta = 0.95),
                                     pars = c("r", "phi", "sigma", "D0", "K")
                                     )
                      }
                    )
  return(all_fit)
}

sim_results <- function(rdata, nom = "SimulSPMStan_", n_sim = 100) {
  ### get a summary of the posterior
  get_summary <- function(x, alpha = 0.2) {
    out <- data.frame(estimate = mean(x),
                      se = sd(x),
                      lower = quantile(x, probs = alpha/2),
                      upper = quantile(x, probs = 1 - alpha/2)
                      )
    return(out)
  }
  
  ### check convergence
  get_result <- function(fit, param) {
    n_chains <- fit@sim$chains
    n_iter <- (fit@sim$iter -fit@sim$warmup) / fit@sim$thin
    x <- rstan::extract(fit, param)
    y <- array(NA, dim = c(length(x), n_iter, n_chains))
    for(i in 1:length(x)) {
      for(j in 1:n_chains) {
        y[i, , j] <- x[[i]][(j - 1) * n_iter + 1:n_iter]
      }
    }
    out <- do.call('rbind', 
                   lapply(rstan::extract(fit, param), get_summary)
                   ) %>%
      mutate(rhat = apply(y, 1, rstan::Rhat),
             param = param
             )
    return(out)
  }
  
  get_results <- function(fitlist, param) {
    out <- do.call('rbind',
                   lapply(fitlist, 
                          get_result, 
                          param = param
                          )
                   ) %>%
      as.data.frame()
    row.names(out) <- NULL
    out$n_survey <- rep(1:length(fitlist), each = length(param))
    return(out)
  }
  ### get environment
  load(paste0("output/", nom, rdata, ".RData"))
  out <- lapply(all_data_fit,
                get_results,
                param = c("r", "phi", "sigma", "D0", "K")
                ) %>%
    do.call('rbind', .) %>%
    mutate(sim_id = rep(n_sim * (rdata - 1) + 1:n_sim, each = 5 * max(n_survey))) %>%
    as.data.frame()
  return(out)
}
