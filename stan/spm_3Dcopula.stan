functions {
  real upper_bound_sigma(real phi, real r, real gamma) {
    // upper bound for sigma given phi, r and gamma
    // phi: extraction rate	
    // r: growth rate
    // gamma: shape of the Pella-Tomlinson DD function
    real value;
    // sanity checks
    if (!(phi >= 0)) {
     reject("upper_bound_sigma(phi, r, gamma): phi must be positive; found phi = ", phi);
    }
    if (!(r >= 0)) {
     reject("upper_bound_sigma(phi, r, gamma): r must be positive; found r = ", r);
    }
    if (!(gamma >= 0)) {
     reject("upper_bound_sigma(phi, r, gamma): gamma must be positive; found gamma = ", gamma);
    }
    value = (1 + inv(gamma)) * log1p(gamma); // numerator
    value += -log(gamma) - log1m(phi + r * (gamma + 1) * inv(gamma)); // denominator
    return sqrt(expm1(value));
  }

  // for Beta likelihood on strandings
  real D_theta_log(real phi, real r, real gamma, real q, real theta0, real B0) {
    // logarithm of D(theta)
    // phi: extraction rate	
    // r: growth rate
    // gamma: shape of the Pella-Tomlinson DD function
    // q: buoyancy probability
    // theta0: initial depletion
    // B0: initial abundance/biomass
    real value;
    // sanity checks
    if (!(phi >= 0)) {
     reject("D_theta_log(phi, r, gamma, q, theta0, B0): phi must be positive; found phi = ", phi);
    }
    if (!(r >= 0)) {
     reject("D_theta_log(phi, r, gamma, q, theta0, B0): r must be positive; found r = ", r);
    }
    if (!(gamma >= 0)) {
     reject("D_theta_log(phi, r, gamma, q, theta0, B0): gamma must be positive; found gamma = ", gamma);
    }
    if (!(q >= 0)) {
     reject("D_theta_log(phi, r, gamma, q, theta0, B0): q must be positive; found q = ", q);
    }
    if (!(q <= 1)) {
     reject("D_theta_log(phi, r, gamma, q, theta0, B0): q must be less than or equal to one; found q = ", q);
    }
    if (!(theta0 >= 0)) {
     reject("D_theta_log(phi, r, gamma, q, theta0, B0): theta0 must be positive; found theta0 = ", theta0);
    }
    if (!(B0 >= 0)) {
     reject("D_theta_log(phi, r, gamma, sigma, q, theta0, B0): B0 must be positive; found B0 = ", B0);
    }
    value = log(B0) - log(theta0) - log(q) - log(phi);
    value += inv(gamma) * (log(r * (gamma + 1)) - log(gamma * (1 - phi + r) + r));
	return value;
  }

  real strandings_lpdf(real[] y, real phi, real r, real gamma, real sigma, real q, real theta0, real B0) {
    // log likelihood for strandings
    // y: data
    // phi: extraction rate	
    // r: growth rate
    // gamma: shape of the Pella-Tomlinson DD function
    // sigma: environmental stochasticity
    // q: buoyancy probability
    // theta0: initial depletion
    // B0: initial abundance/biomass
    int n = size(y); // length of time series for y
    real coef;
    real value;
    // sanity checks
    if (!(n >= 2)) {
     reject("strandings_lpdf(y, phi, r, gamma, sigma, q, theta0, B0): y must be of size greater than one; found size = ", n);
    }
    if (!(phi >= 0)) {
     reject("strandings_lpdf(y, phi, r, gamma, sigma, q, theta0, B0): phi must be positive; found phi = ", phi);
    }
    if (!(r >= 0)) {
     reject("strandings_lpdf(y, phi, r, gamma, sigma, q, theta0, B0): r must be positive; found r = ", r);
    }
    if (!(gamma >= 0)) {
     reject("strandings_lpdf(y, phi, r, gamma, sigma, q, theta0, B0): gamma must be positive; found gamma = ", gamma);
    }
    if (!(sigma >= 0)) {
     reject("strandings_lpdf(y, phi, r, gamma, sigma, q, theta0, B0): sigma must be positive; found sigma = ", sigma);
    }
    if (!(q >= 0)) {
     reject("strandings_lpdf(y, phi, r, gamma, sigma, q, theta0, B0): q must be positive; found q = ", q);
    }
    if (!(q <= 1)) {
     reject("strandings_lpdf(y, phi, r, gamma, sigma, q, theta0, B0): q must be less than or equal to one; found q = ", q);
    }
    if (!(theta0 >= 0)) {
     reject("strandings_lpdf(y, phi, r, gamma, sigma, q, theta0, B0): theta0 must be positive; found theta0 = ", theta0);
    }
    if (!(B0 >= 0)) {
     reject("strandings_lpdf(y, phi, r, gamma, sigma, q, theta0, B0): B0 must be positive; found B0 = ", B0);
    }
    coef = exp(log(theta0) - log(q) - log(phi) - log(B0));
    value = 0.0;
    // currently does not include the first datum
    for(t in 2:n) {
      real mu = log(y[t-1] + ((gamma + 1) * r / gamma) * y[t-1] * (1 - pow(coef * y[t-1], gamma)) - phi * y[t-1]) - 0.5 * square(sigma);
      value += lognormal_lpdf(y[t] | mu, sigma);
    }
    return value;
  }

  real biomass_lpdf(real[] y, real phi, real q) {
    // log likelihood for abundance/biomass given strandings
    // y: vector of length 3, with 1-strandings, 2-estimated abundance/biomass and 3-aasociated coefficient of variation
    // phi: extraction rate
    // q: buoyancy probability
    int n = size(y); // length of time series for y
    real tau; // scale parameter of log-normal distribution
    real mu;  // location parameter of log-normal distribution
    
    // sanity checks
    if (!(n == 3)) {
     reject("biomass_lpdf(y, phi, q): y must be of size 3; found size = ", n);
    }
    if (!(phi >= 0)) {
     reject("biomass_lpdf(y, phi, q): phi must be positive; found phi = ", phi);
    }
    if (!(q >= 0)) {
     reject("biomass_lpdf(y, phi, q): q must be positive; found q = ", q);
    }
    if (!(q <= 1)) {
     reject("biomass_lpdf(y, phi, q): q must be less than or equal to one; found q = ", q);
    }
    tau = sqrt(log1p(y[3] * y[3]));
    mu = log(y[1]) - log(q) - log(phi) - 0.5 * square(tau);
    return lognormal_lpdf(y[2] | mu, tau);
  }

  // Gumbel copula lpdf from https://spinkney.github.io/helpful_stan_functions/group__gumbel.html
  real gumbel_copula(real u, real v, real theta) {
    real neg_log_u = -log(u);
    real log_neg_log_u = log(neg_log_u);
    real neg_log_v = -log(v);
    real log_neg_log_v = log(neg_log_v);
    real log_temp = log_sum_exp(theta * log_neg_log_u, theta * log_neg_log_v);
    real theta_m1 = theta - 1;
    if (theta < 1) {
      reject("theta must be >= 1, found theta = ", theta);
    }
    if (is_inf(theta)) {
      if (u == v) {
        return 0;
      }
      else{
        return negative_infinity();
      }
    }
  
    return theta_m1 * log_neg_log_u + theta_m1 * log_neg_log_v
     + neg_log_u + neg_log_v - exp(log_temp / theta)
     + log_sum_exp(2 * theta_m1 / -theta * log_temp, log(theta_m1) + (1 - 2 * theta) / theta * log_temp);
  }
  // jacobian 
  real jacobian_gumbel_copula(real u, real v, real theta){
    real neg_log_u = -log(u);
    real psi_u = pow(neg_log_u, theta);
    real neg_log1m_u = -log1m(u);
    real psi_1mu = pow(neg_log1m_u, theta);
    real neg_log_v = -log(v);
    real psi_v = pow(neg_log_v, theta);
    real value;

    value = lmultiply(theta - 1,  neg_log_u) - log(u);
    value += lmultiply(inv(theta) - 1, psi_1mu + psi_v);
    value += -pow(psi_1mu + psi_v, -inv(theta));
    return value;
  }

  // Gaussian copula lpdf from https://spinkney.github.io/helpful_stan_functions/group__normal.html
  real normal_copula(real u, real v, real rho) {
    real rho_sq = square(rho);
  
    return (0.5 * rho * (-2. * u * v + square(u) * rho + square(v) * rho)) / (-1. + rho_sq)
           - 0.5 * log1m(rho_sq);
  }
  // jacobian
  real jacobian_normal_copula(real u, real v, real theta){
    real value = Phi((inv_Phi(u) - theta * inv_Phi(v)) * inv_sqrt(1 - square(theta)));
    if (value == 0) {
      return negative_infinity();
    }
    else{
      return log(value);
    }
  }

  // Tawn Type 2 copula lpdf: see Ouzoulias 2022
  real tawn_2_copula(real u, real v, real theta1, real theta2) {
    real log_uv; // is negative
    real t; // is positive and less than 1
    vector[2] temp[3];
    real t_prime_u; // is positive
    real t_prime_v;
    real t_second;
    vector[3] alpha;
    real f_t_prime_u;
    real f_t_prime_v;
    real value;
    // sanity checks
    if (theta1 <= 1) {
     reject("theta1 must be greater than 1; found theta1 = ", theta1);
    }
    if (theta2 < 0) {
     reject("theta2 must be greater than or equal to 0; found theta2 = ", theta2);
    }
    if (theta2 > 1) {
     reject("theta2 must be smaller than or equal to 1; found theta2 = ", theta2);
    }
    // intermediate computations to b stored for efficiency
    log_uv = log(u * v); 
    t = lmultiply(inv(log_uv), v); // log(v) * inv(log_uv);
    for(i in 1:3) {
      temp[i, 1] = pow(t, theta1 + 1 - i);
      temp[i, 2] = pow(1 - t, theta1 + 1 - i);
    }
    t_prime_u = -lmultiply(inv(u), v) * inv_square(log_uv); // is positive
    //t_prime_u = -log(v) * inv(u * square(log_uv)); // is positive
    t_prime_v = (lmultiply(inv(v), u * v) - lmultiply(inv(u), v)) * inv_square(log_uv); // is real;
    //t_prime_v = (inv(v) * log_uv - inv(u) * log(v)) * inv_square(log_uv); // is real
    t_second = -inv(u * v * pow(log_uv, 3)) * (log_uv - 2 * log(v)); // is real
    alpha[1] = (1 - theta1) * inv_square(theta1) * pow(theta2 * temp[1, 2] + temp[1, 1], inv(theta1) - 2);
    alpha[2] = -inv(theta1) * pow(theta2 * temp[1, 2] + temp[1, 1], inv(theta1) - 1);
    alpha[3] = -t_second * theta1 * (theta2 * temp[2, 2] - 1) + t_prime_u * t_prime_v * theta1 * (theta1 - 1) * (theta2 * temp[3, 2] + temp[3, 1]);
    f_t_prime_u = t_prime_u * theta1 * (temp[2, 1] - theta2 * temp[2, 2]);
    f_t_prime_v = t_prime_v * theta1 * (temp[2, 1] - theta2 * temp[2, 2]);

    // probability density function
    value = (1 - theta2) * t_second + alpha[1] * f_t_prime_u * f_t_prime_v + alpha[2] * alpha[3];
    if (value <= 0) {
      return negative_infinity();
    }
    else{
      return log(value);
    }
  }
}

data {
  int<lower = 2> n_strandings;
  int<lower = 1> n_surveys;
  real<lower = 0.0> STRANDINGS[n_strandings]; // strandings time series
  real<lower = 0.0> SURVEY[n_surveys, 3];     // abundance or biomass
  real<lower = 0.0> gamma;         // shape of the Pella-Tomlinson DD function
  real<lower = 0.0> B0;            // B0: initial abundance/biomass
  real<lower = 0.0, upper = 1.0> q;// buoyancy probability
  real<lower = 0.0> upper_bound_phi; // upper bound for phi
  real<lower = 0.0> upper_bound_r;   // upper bound for r
  real<lower = 0.0> upper_bound4sigma; // upper bound for sigma
  real<lower = 0.0> lower_bound_D0; // lower bound for D0
  real<upper = -1.0> theta_RG; // Rotated Gumbel copula dependence parameter
  real<lower = -1.0, upper = 1.0> theta_G; // Gaussian copula dependence parameter
  vector<lower = 0.0>[2] theta_RT; // Rotated Tawn 2 copula dependence parameters
  int<lower = 0, upper = 1> COPULA; // indicator for the 3Dcopula prior
}
//
//transformed data {
//	
//}

parameters {
  real<lower = 0.0, upper = 1.0> unscaled_r;    // growth rate
  real<lower = 0.0, upper = 1.0> unscaled_phi;  // extraction rate
  real<lower = 0.0, upper = 1.0> unscaled_sigma;// environmental stochasticity
  real<lower = lower_bound_D0, upper = 1.0> D0; // D0: initial depletion
}

transformed parameters {
  real r = unscaled_r * upper_bound_r;  // growth rate
  real phi = unscaled_phi * upper_bound_phi;  // extraction rate
  real sigma = unscaled_sigma * upper_bound4sigma;// environmental stochasticity
}

model {
  // marginal priors
  unscaled_r ~ uniform(0.0, 1.0);
  unscaled_phi ~ uniform(0.0, 1.0);
  unscaled_sigma ~ uniform(0.0, 1.0);
  // copulas: use the cdf
  // Rotated Gumbel
  target += COPULA == 0 ? 0 : gumbel_copula(1 - unscaled_phi, unscaled_sigma, -theta_RG);
  target += COPULA == 0 ? 0 : jacobian_gumbel_copula(unscaled_phi, unscaled_sigma, theta_RG);
  // Gaussian Copula
  target += COPULA == 0 ? 0 : normal_copula(unscaled_phi, unscaled_r, theta_G);
  target += COPULA == 0 ? 0 : jacobian_normal_copula(unscaled_phi, unscaled_r, theta_G);
  // Rotated Tawn type 2 
  target += COPULA == 0 ? 0 : tawn_2_copula(1 - unscaled_r, 1 - unscaled_sigma, theta_RT[1], theta_RT[2]);
  // likelihood
  // strandings
  target += strandings_lpdf(STRANDINGS| phi, r, gamma, sigma, q, D0, B0);
  // surveys
  for(i in 1:n_surveys) {
  	target += biomass_lpdf(SURVEY[i, 1:3]| phi, q);
  }
}

generated quantities {
  real K = B0 * inv(D0);
}
