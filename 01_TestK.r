##--------------------------------------------------------------------------------------------------------
## SCRIPT : Test Stan on Stochastic MSY model with theta0
##
## Authors : Matthieu Authier, Nicolas Bousquet & Fanny Ouzoulias
## Last update : 2022-06-22
##
## R version 4.0.5 (2021-03-31) -- "Shake and Throw"
## Copyright (C) 2021 The R Foundation for Statistical Computing
## Platform: x86_64-w64-mingw32/x64 (64-bit)
##--------------------------------------------------------------------------------------------------------

lapply(c("tidyverse", "lubridate", "rstan", "loo"), 
       library, character.only = TRUE
       )

### check inverse gamma prior
igam_shape <- 65
igam_scale <- 0.4 # be careful here between scale and rate parametrization
ir <- rgamma(1e6, shape = igam_shape, scale = igam_scale)
hist(1/ir)
summary(1/ir)

rm(list = ls())

### simulation de données
source("R/functions/functions_echouages_07-06-2022.R")   # fonctions pour mettre en place le Gibbs

set.seed(20220622)
### length of time series
n_data <- 100 # nb d'années de captures 
### measurement error
cv <- 0.1
tau <- sqrt(log1p(cv * cv))
### carrying capacity
K <- 5e+05
theta0 <- 0.9 # initial condition
r <- 0.04 # taux de croissance intrinsèque
phi <- 0.005 # taux d'extraction
gamma <- 2.4 # paramètre SPM
q <- 1.0 # fraction échouages bycatch

alpha <- 1 # not used in code
sigma_square <- sqrt(0.01) # stochasticité environnementale

simul_data <- spm.data.generation(N = n_data,
                                  theta1 = theta0, 
                                  r = r, 
                                  phi = phi,
                                  sigma.square = sigma_square,
                                  gamma = gamma,
                                  q = q,
                                  alpha = alpha,
                                  cv.B.obs = cv, 
                                  B0.obs = rlnorm(1, 
                                                  meanlog = log(K * theta0) - 0.5 * tau * tau, 
                                                  sdlog = tau
                                                  ) %>%
                                    floor()  
                                  ) %>%
  do.call('cbind', .) %>%
  as.data.frame() %>%
  mutate(time = 1:n()) %>%
  pivot_longer(cols = -time,
               names_to = "param",
               values_to = "value"
               )

simul_data %>%
  ggplot(aes(x = time, y = value, group = param, color = param)) +
  geom_line() +
  scale_y_log10() +
  theme_bw()

### Stan
rstan_options(auto_write = TRUE)
options(mc.cores = parallel::detectCores())
spm <- stan_model(file = "stan/Kspm_unscaledprior.stan", 
                  model_name = "Stochastic MSY with prior on theta0"
                  )

stan_data <- list(n_strandings = n_data,
                  n_surveys = length(c(1, seq(10, 100, 10))),
                  STRANDINGS = simul_data %>%
                    filter(param == "Ce") %>%
                    pull(value),
                  SURVEY = simul_data %>%
                    filter(time %in% c(1, seq(10, 100, 10))) %>%
                    pivot_wider(names_from = param,
                                values_from = value
                                ) %>%
                    select("Ce", "Bt.obs") %>%
                    mutate(cv = cv) %>%
                    as.matrix(),
                  gamma = gamma,
                  # use the first estimate
                  B0 = simul_data %>%
                    filter(time == 1,
                           param == "Bt.obs"
                           ) %>%
                    pull(value),
                  q = 1.0,
                  upper_bound_r = 0.1,
                  upper_bound_phi = 0.1,
                  lower_bound_theta0 = 0.5
                )

### test
fit = sampling(spm, 
               data = stan_data, 
               iter = 1000, 
               warmup = 500, 
               thin = 1, 
               chains = 4, 
               control = list(max_treedepth = 15, adapt_delta = 0.95),
               pars = c("r", "phi", "sigma", "theta0")
               )


stan_rhat(fit)

plot(fit,
     plotfun = "stan_trace",
     pars = c("r", "phi", "sigma", "theta0"),
     inc_warmup = TRUE
     )

print(fit, digits = 4)

plot(fit,
     plotfun = "stan_hist",
     pars = c("r", "phi", "sigma", "theta0"),
     inc_warmup = FALSE
     )

plot(fit,
     plotfun = "stan_dens",
     pars = c("r", "phi", "sigma", "theta0"),
     inc_warmup = FALSE
     )

pairs(fit)

### try a Gumbel copula just for fun
spm_gumbel <- stan_model(file = "stan/Kspm_unscaledprior_gumbelcopula.stan", 
                  model_name = "Stochastic MSY"
                  )

stan_data$kendalls_tau <- 0.7
fit2 = sampling(spm_gumbel, 
                data = stan_data, 
                iter = 1000, 
                warmup = 500, 
                thin = 1, 
                chains = 4, 
                control = list(max_treedepth = 15, adapt_delta = 0.95),
                pars = c("r", "phi", "sigma", "theta0")
                )

stan_rhat(fit2)

plot(fit2,
     plotfun = "stan_trace",
     pars = c("r", "phi", "sigma", "theta0"),
     inc_warmup = TRUE
     )

print(fit2, digits = 4)

### try an informative inverse gamma prior on r --> pb on prior here
### try an informative gamma prior on r
spm_gamma <- stan_model(file = "stan/Kspm_gam.stan", 
                        model_name = "Stochastic MSY"
                        )

fit3 = sampling(spm_gamma, 
                data = list(n_strandings = n_data,
                            n_surveys = length(c(1, seq(10, 100, 10))),
                            STRANDINGS = simul_data %>%
                              filter(param == "Ce") %>%
                              pull(value),
                            SURVEY = simul_data %>%
                              filter(time %in% c(1, seq(10, 100, 10))) %>%
                              pivot_wider(names_from = param,
                                          values_from = value
                                          ) %>%
                              select("Ce", "Bt.obs") %>%
                              mutate(cv = cv) %>%
                              as.matrix(),
                            gamma = gamma,
                            # use the first estimate
                            B0 = simul_data %>%
                              filter(time == 1,
                                     param == "Bt.obs"
                                     ) %>%
                              pull(value),
                            q = 1.0,
                            upper_bound_phi = 0.1,
                            lower_bound_theta0 = 0.5
                            ), 
                iter = 1000, 
                warmup = 500, 
                thin = 1, 
                chains = 4, 
                control = list(max_treedepth = 15, adapt_delta = 0.95),
                pars = c("r", "phi", "sigma", "theta0", "check", "sigma_max")
                )

stan_rhat(fit3)

plot(fit3,
     plotfun = "stan_trace",
     pars = c("r", "phi", "sigma", "theta0", "check", "sigma_max"),
     inc_warmup = TRUE
     )

print(fit3, digits = 4)
