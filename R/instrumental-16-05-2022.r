

#====================================#
# The Truncated Normal Distribution
#====================================#

# density
dtnorm <- function(x, mean = 0, sd = 1, min = -1e6, max = 1e6){
  out = dnorm(x, mean, sd) / (pnorm(max, mean, sd) - pnorm(min, mean, sd))
  out[(x < min) | (x > max)] = 0
  return(out)
}

# distribution function
ptnorm <- function(q, mean = 0, sd = 1, min = -1e6, max = 1e6){
  out = (pnorm(q, mean, sd) - pnorm(min, mean, sd)) /
    (pnorm(max, mean, sd) - pnorm(min, mean, sd))
  out[q < min] = 0
  out[q > max] = 1
  return(out)
}

# quantile function
qtnorm <- function(p, mean = 0, sd = 1, min = -1e6, max = 1e6){
  return(qnorm((1 - p) * pnorm(min, mean, sd) + p * pnorm(max, mean, sd),
               mean, sd))
}

# random generation
rtnorm <- function(n, mean = 0, sd = 1, min = -1e6, max = 1e6){
  return(qtnorm(runif(n), mean, sd, min, max))
}

#=======================================
# The Truncated Lognormal Distribution
#=======================================

# density
dtlnorm <- function(x, meanlog = 0, sdlog = 1, min = -1e6, max = 1e6){
  out = dlnorm(x, meanlog, sdlog) / (plnorm(max, meanlog, sdlog) - plnorm(min, meanlog, sdlog))
  out[(x < min) | (x > max)] = 0
  return(out)
}

# distribution function
ptlnorm <- function(q, meanlog = 0, sdlog = 1, min = -1e6, max = 1e6){
  out = (plnorm(q, meanlog, sdlog) - plnorm(min, meanlog, sdlog)) /
    (plnorm(max, meanlog, sdlog) - plnorm(min, meanlog, sdlog))
  out[q < min] = 0
  out[q > max] = 1
  return(out)
}

# quantile function
qtlnorm <- function(p, meanlog = 0, sdlog = 1, min = -1e6, max = 1e6){
  return(qlnorm((1 - p) * plnorm(min, meanlog, sdlog) + p * plnorm(max, meanlog, sdlog),
               meanlog, sdlog))
}

# random generation
rtlnorm <- function(n, meanlog = 0, sdlog = 1, min = -1e6, max = 1e6){
  return(qtlnorm(runif(n), meanlog, sdlog, min, max))
}


