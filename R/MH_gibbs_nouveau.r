## Metropolis Hasting within Gibbs

################ Simulation des données #############################
n_data <-50
n <- 3 # nbre de chaînes MCMC
mu.sim <- 2
sigma.sim <- 1
data <- x <- rnorm(n_data, mean = mu.sim, sd = sigma.sim)
X <- matrix(rep(data,n), length(data), n) # matrice avec colonnes identiques (trois chaînes)

################ Paramètres ########################################

tau <- 1
M=1000 # nombre d'itérations dans la boucle 

################ Priors ###########################################
# Prior sur mu : uniforme
a <- 1
b <- 2
mu.prior <- runif(n, a, b)

# Prior sur sigma : exponentielle
lambda <- 5
sigma.prior <- rexp(n, rate = lambda)

######## Fonctions relatives au calcul du rapport Metropolis #############

##################### mu ##############################################

# Simulation instrumentale pour mu : loi normale (marche aléatoire)
  rinstr.mu <- function(n,mu,tau)
  {
    rnorm(n,mean=mu,sd=tau)    # paramétrisation de la loi gaussienne sd = TAU 
  }

# Log densité instrumentale sur mu
log.dinstr.mu <- function(x,mu,tau) # fonction de LOG-densité loi normale
{
  res <- -0.5*((x-mu)^2/(tau^2)) - log(sqrt(2*pi)) - log(tau)
  return(res)
}

# Log densite loi a priori jointe
# attention ici x = vecteur des sigma
log.dens.prior <- function(x,lambda)
{
  # log prior sur mu
  res.mu <- 0
  # log prior sur sigma
  res.sigma <- log(lambda) - x*lambda
  res <- res.mu + res.sigma
  return(res)
}

# Log densité loi à posteriori sur mu conditionnelle à sigma et aux données
# mu et sigma = 2 vecteurs de taille n 
# x = vecteur de mu courant de taille n également

log.likelihood <- function(mu, sigma,x=data)
{
  MU <- t(matrix(rep(mu, length(x)), n, length(x))) # matrice des mu tirés initialement 
  X <- matrix(rep(x,n), length(x), n)
  res <- -log((sigma*sqrt(2*pi))^n_data)*(-0.5^n_data)-(apply((X-MU)^2, 2, sum,na.rm=T)/(sigma^2))  
  return(res)
}


log.post.mu <- function(mu, sigma,x=data)
{
  res <- log.likelihood(mu, sigma, data) # log densité prior nul pour mu (loi uniforme)
  return(res)
}  

########################## sigma ####################################

# Simulation instrumentale pour sigma
rinstr.sigma <- function(n, sigma, tau)
{
  # rlnorm(n, mean= log(sigma) - (tau^2/2), sd=tau)
  # location parameter of log-normal variate with expectation sigma
  mu <- log(sigma) - (tau^2/2)
  res <- rlnorm(n, mu, tau)
  return(res)
}

# Log densité instrumentale
log.dinstr.sigma <- function(x, sigma, tau)
{
  #res <- -(x-sigma)^2/(2*tau^2)
  # location parameter of log-normal variate with expectation sigma
  mu <- log(sigma) - (tau^2/2)
  # log density of log-normal variate with location parameter mu and scale parameter tau
  res <- -log(x) - 2 * log(tau) - log(sqrt(2*pi)) - (((log(x) - mu)^2) / (2*tau^2))
  return(res)
}

# Log densité loi à posteriori

# mu et sigma = 2 vecteurs de taille n 
# x = vecteur de mu courant de taille n également

log.post.sigma <- function(x, mu, sigma)
{
  res <- log.likelihood(mu, sigma, x=data) + log.dens.prior(sigma, lambda) # 2 premiers termes = vraisemblance
  return(res)
}    

####### Initialisation ###################

# Initialisation de mu
mu <- rnorm(n, 20, 1)

# Initialisation de sigma
sigma <- rlnorm(n, 1, 0.0001)


##### outils pour la conservation des tirages #####
out.mu <- matrix(mu, ncol = n)
out.sigma <- matrix(sigma, ncol=n)
accept <- matrix(NA, ncol = n)
#mu.hist <- c()

############################ boucle ######################################

k <- 0
while (k<M)
{
  k <-k+1
 
#====================================================
# Step 1 : MH pour mu sachant sigma
# mu.hist <- rbind(mu.hist,mu) # sauvegarde du mu courant
  
# Simulation instrumentale
mu.tilde <- rinstr.mu(n,mu,tau)
  
# Calcul du log-rapport de metropolis
res1 <- log.post.mu(mu,mu.tilde,sigma) - log.post.mu(mu.tilde,mu,sigma)
res2 <- log.dinstr.mu(mu,mu.tilde,tau) - log.dinstr.mu(mu.tilde,mu,tau)
log.ratio <- res1 + res2
#print(log.ratio);flush.console()
  
mu.last <- mu

# Proposition 
mu <- mu.tilde
# Acceptation 
U <- runif(n, 0, 1)
test <- (log(U) <= log.ratio)
# Mise à jour (mu = mu.tilde avec proba log.ratio)
mu[!test] <- mu.last[!test]
#print(paste("% d'acceptation = ",sum(test)/n));flush.console()
  
#====================================================
# Step 2 : MH pour sigma sachant mu

# Simulation instrumentale
sigma.tilde <- rinstr.sigma(n,sigma,tau)
print(paste("sigma = ",sigma))
print(paste("sigma.tilde = ",sigma.tilde))
print("==============")

  
# Calcul du log-rapport de metropolis
res1.sigma <- log.post.sigma(sigma,mu,tau) - log.post.sigma(sigma.tilde,mu,tau) 
res2.sigma <- log.dinstr.sigma(sigma,sigma.tilde,tau) - log.dinstr.sigma(sigma.tilde,sigma,tau)
log.ratio.sigma <- res1.sigma + res2.sigma
  
# Proposition
sigma <- sigma.tilde
sigma.last <- sigma
# Acceptation 
U <- runif(n, 0, 1)
test <- (log(U) <= log.ratio.sigma)
# Mise à jour de sigma
sigma[!test] <- sigma.last[!test]
print(paste("% d'acceptation sur sigma = ",sum(test)/n));flush.console()
  
  
# Conservation des anciens tirages de mu et sigma
out.mu <- rbind(out.mu,
               matrix(mu, ncol = n)
               )
  
  
out.sigma <- rbind(out.sigma,
                  matrix(sigma, ncol = n)
  )
  
accept <- rbind(accept,
                  test
                  )

}

# plot
#layout(matrix(c(1:2),2,1))
#layout.show(2)
matplot(x=0:k, y=out.mu,type="l")
matplot(x=0:k, y=out.sigma,type="l", ylim=c(0, 50))

#matplot(x=0:k, y=out.mu,type="l",xlim=c(0,M),ylim=c(-2,5))
#matplot(x=0:k, y=out.sigma,type="l",xlim=c(0,M),ylim=c(0,1))







