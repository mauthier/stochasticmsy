##--------------------------------------------------------------------------------------------------------
## SCRIPT : Fonction de vraisemblance
##
## Authors : Fanny Ouzoulias
## Last update : 2022-02-23
##
## R version 3.6.3 (2020-02-29) -- "Holding the Windsock"
## Copyright (C) 2020 The R Foundation for Statistical Computing
## Platform: x86_64-pc-linux-gnu (64-bit)
##--------------------------------------------------------------------------------------------------------

# Paramètres
# se référer au script "simul_biomass"

K <- 5e5
r <- 0.4
tau <- 0.1 # écart type sur les observations de captures
phi_value <- 0.1
C0 <- C[1,1] # C = phi B (cf résultat du script simulant les biomasses)
rho <- r - phi_value

#résoudre le système ? cf p. 6 doc programme de travail 
alpha <- 1/(2*(1+rho)*sd_env^2)*(((2/delta)-rho-(1+rho)*sd_env)+sqrt(4*sd_env*(1+delta)/delta^2)+((2/delta)-rho-(1+rho)*sd_env)^2)
delta <- (alpha+1)/(rho*beta)
beta <- delta*rho*beta -1
# paramètres de la loi beta strictement positifs
  

# expression fonction 1 : f(Ct* | Zt)
# On pose x = Z0
f1 <- function(x) {
  
(1/(C0*sqrt((phi_value*K*(1+r-phi_value)))/r)*sqrt(x)*tau)*exp(-r/(2*phi_value*K*(1+phi_value+r)*x*tau^2)*(log(C0)+(tau^2/2)*((phi_value*K*(1+r-phi_value)*x)/r))^2)

  }

# expression fonction 2 : f(Zt | theta)
# x = Z0
# Zt suit loi bêta*


f2 <- function(x) {

(1/beta(alpha, beta))*x^(alpha-1)*(1-x)^(beta-1)
      
}

f3 <-  f1 * f2 # multiplier des fonctions ?

integrate(f3,lower=0,upper=1)$value






