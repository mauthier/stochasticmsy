## Metropolis Hasting within Gibbs

################ Simulation des donnees #############################
n_data <-50
n <- 3 # nbre de chaines MCMC
mu.sim <- 2
sigma.sim <- 1
data <- x <- rnorm(n_data, mean = mu.sim, sd = sigma.sim)
X <- matrix(rep(data,n), length(data), n) # matrice avec colonnes identiques (trois chaînes)

################ Paramètres ########################################

tau <- 1
M=1000 # nombre d'iterations dans la boucle MCMC

# Prior sur sigma : exponentielle de parametre lambda
lambda <- 2 # l'esperance a priori de sigma est 1/lambda


######## Fonctions relatives au calcul du rapport Metropolis #############

##################### mu ##############################################

# Simulation instrumentale pour mu : loi normale (marche aléatoire)
  rinstr.mu <- function(n,mu)
  {
    rnorm(n,mean=mu,sd=tau)    # paramétrisation de la loi gaussienne sd = TAU 
  }

# Log densité instrumentale sur mu
log.dinstr.mu <- function(x,mu) # fonction de LOG-densité loi normale
{
  res <- -0.5*((x-mu)^2/(tau^2)) - log(sqrt(2*pi)) - log(tau)
  return(res)
}

# Log densite loi a priori sur sigma
log.dens.prior.sigma <- function(x)
{
  # log prior sur sigma (log exponentielle)
  res <- log(lambda) - x*lambda
  return(res)
}

# Log densite loi a priori sur mu (log uniforme)
log.dens.prior.mu <- function(x)
{
  res <- rep(0,length(x))
  return(res)
}



########################## sigma ####################################

# Simulation instrumentale etape n pour sigma
# ici l'argument sigma indique une valeur a l'etape n-1 
rinstr.sigma <- function(n, sigma)
{

  y <- log(sigma) - (tau^2/2) # moyenne de la loi instrumentale sur log(sigma)
  v <- tau # ecart-type de la loi instrumentale sur log(sigma)
  log.sigma <- rnorm(n,y,v) # simulation de log(sigma)
  res <- exp(log.sigma)  # simulation de sigma
  return(res)
}

# Log densité instrumentale sur sigma
log.dinstr.sigma <- function(x, sigma)
{

  y <- log(sigma) - (tau^2/2) # parametre de localisation de la lognormale
  v <- tau # parametre d'echelle
  # log density of log-normal variate with location parameter mu and scale parameter tau
  res <- -log(x) - log(tau)  - (((log(x) - y)^2) / (2*tau^2))
  return(res)
}



########################## vraisemblance et posterior joint ####################################


log.likelihood <- function(mu, sigma)
{
  MU <- t(matrix(rep(mu, length(data)), n, length(data))) 
  res <-  -log(sigma) -(apply((X-MU)^2, 2, sum,na.rm=T)/(2*sigma^2))
  return(res)
}


log.post <- function(mu, sigma)
{
  res <- log.likelihood(mu, sigma) + log.dens.prior.mu(mu) + log.dens.prior.sigma(sigma)
  return(res)
}  


   

####### Initialisation ###################

# Initialisation de mu
mu <- rnorm(n, 20, 1)

# Initialisation de sigma
sigma <- rlnorm(n, 1, 0.0001)


##### outils pour la conservation des tirages #####
out.mu <- matrix(mu, ncol = n)
out.sigma <- matrix(sigma, ncol=n)
accept <- matrix(NA, ncol = n)
#mu.hist <- c()

############################ boucle ######################################

k <- 0
while (k<M)
{
  k <-k+1
 
  #====================================================
  # Step 1 : MH pour mu sachant sigma
  # mu.hist <- rbind(mu.hist,mu) # sauvegarde du mu courant
  
  # Simulation instrumentale
  mu.tilde <- rinstr.mu(n,mu)
  
  # Calcul du log-rapport de metropolis
  res1 <- log.post(mu.tilde,sigma) - log.post(mu,sigma)
  res2 <- log.dinstr.mu(mu,mu.tilde) - log.dinstr.mu(mu.tilde,mu)
  log.ratio <- res1 + res2
  #print(log.ratio);flush.console()



  # MAJ------------------------------
  mu.last <- mu
  mu <- mu.tilde
  # Acceptation 
  U <- runif(n, 0, 1)
  test <- (log(U) <= log.ratio)
  # protection num?rique
  test[is.na(test)]=0
  # Mise à jour (mu = mu.tilde avec proba log.ratio)
  mu[!test] <- mu.last[!test]
  #print(paste("% d'acceptation = ",sum(test)/n));flush.console()
  
  #====================================================
  # Step 2 : MH pour sigma sachant mu

  # Simulation instrumentale
  sigma.tilde <- rinstr.sigma(n,sigma)
  
  # Calcul du log-rapport de metropolis
  res1.sigma <- log.post(mu,sigma.tilde) - log.post(mu,sigma) 
  res2.sigma <- log.dinstr.sigma(sigma,sigma.tilde) - log.dinstr.sigma(sigma.tilde,sigma)
  log.ratio.sigma <- res1.sigma + res2.sigma

  # MAJ ------------------------------
  sigma.last <- sigma
  sigma <- sigma.tilde
  # Acceptation 
  U <- runif(n, 0, 1)
  test <- (log(U) <= log.ratio.sigma)
  # Mise à jour de sigma
  sigma[!test] <- sigma.last[!test]
  #print(paste("% d'acceptation sur sigma = ",sum(test)/n));flush.console()
  



  
  # Conservation des anciens tirages de mu et sigma
  out.mu <- rbind(out.mu,
               matrix(mu, ncol = n)
               )
  
  
  out.sigma <- rbind(out.sigma,
                  matrix(sigma, ncol = n)
   )
  
  accept <- rbind(accept,
                  test
                  )

}


#layout(matrix(c(1:2),2,1))
#layout.show(2)

matplot(x=0:k, y=out.mu,type="l",xlab="nb iterations",ylab="mu")
matplot(x=0:k, y=out.sigma,type="l", ylim=c(0, 50),xlab="nb iterations",ylab="sigma")









