#====================================#
# The Truncated Normal Distribution
#====================================#

# density
dtnorm <- function(x, location = 0, scale = 1, left = -1e6, right = 1e6) {
  out = dnorm(x, location, scale) / (pnorm(right, location, scale) - pnorm(left, location, scale))
  out[(x < left) | (x > right)] = 0
  return(out)
}

# distribution function
ptnorm <- function(q, location = 0, scale = 1, left = -1e6, right = 1e6) {
  out = (pnorm(q, location, scale) - pnorm(left, location, scale)) /
    (pnorm(right, location, scale) - pnorm(left, location, scale))
  out[q < left] = 0
  out[q > right] = 1
  return(out)
}

# quantile function
qtnorm <- function(p, location = 0, scale = 1, left = -1e6, right = 1e6) {
  out <- qnorm((1 - p) * pnorm(left, location, scale) + p * pnorm(right, location, scale),
               location, scale
               )
  return(out)
}

# random generation
rtnorm <- function(n, location = 0, scale = 1, left = -1e6, right = 1e6) {
  out <- qtnorm(runif(n), location, scale, left, right)
  return(out)
}

#=======================================
# The Truncated Lognormal Distribution
#=======================================

# density
dtlnorm <- function(x, location = 0, scale = 1, left = -1e6, right = 1e6) {
  out = dlnorm(x, meanlog = location, sdlog = scale) / (plnorm(right, meanlog = location, sdlog = scale) - plnorm(left, meanlog = location, sdlog = scale))
  out[(x < left) | (x > right)] = 0
  return(out)
}

# distribution function
ptlnorm <- function(q, location = 0, scale = 1, left = -1e6, right = 1e6) {
  out = (plnorm(q, meanlog = location, sdlog = scale) - plnorm(left, meanlog = location, sdlog = scale)) /
    (plnorm(right, meanlog = location, sdlog = scale) - plnorm(left, meanlog = location, sdlog = scale))
  out[q < left] = 0
  out[q > right] = 1
  return(out)
}

# quantile function
qtlnorm <- function(p, location = 0, scale = 1, left = -1e6, right = 1e6) {
  out <- qlnorm((1 - p) * plnorm(left, meanlog = location, sdlog = scale) + p * plnorm(right, meanlog = location, sdlog = scale),
                meanlog = location, sdlog = scale
                )
  return(out)
}

# random generation
rtlnorm <- function(n, location = 0, scale = 1, left = -1e6, right = 1e6) {
  out <- qtlnorm(runif(n), location, scale, left, right)
  return(out)
}

#====================================#
# LOGKLIKELIHOOD OF STRANDINGS
#====================================#

log_lik_strandings <- function(y, 
                               q_buoy, 
                               phi, 
                               r_max,
                               theta0, 
                               gamma_pt, 
                               sigma_env,
                               B0
                               ) {
  mu <- exp(log(theta0) - log(q_buoy) - log(phi) - log(B0))
  lpdf <- rep(0, length(y))
  # no model for the initial condition
  for (t in c(2:length(y))) {
    g_prov <- log(y[t-1] + 
                    ((gamma_pt+1) * r_max / gamma_pt) * y[t - 1] * (1 - (mu * y[t - 1])^gamma_pt) - phi * y[t-1]
                  ) - 0.5 * sigma_env * sigma_env
    
    lpdf[t] <- log(dtlnorm(y[t], location = g_prov, scale = sigma_env, left = 0))
  }
  lpdf[is.na(lpdf)] <- -Inf
  out <- sum(lpdf)
  return(lpdf)
}

#====================================#
# Data Simulation Model
#====================================#

spm_sampling <- function(n_obs,
                         theta0, 
                         r_max, 
                         phi,
                         sigma_env,
                         gamma_pt,
                         q_buoy,
                         cv,
                         B0
                         ) {
  tau <- sqrt(log1p(cv * cv))
  mu <- exp(log(theta0) - log(q_buoy) - log(phi) - log(B0))
  out <- NULL
  strandings <- abundance <- rep(NA, n_obs)
  # initial condition
  abundance[1] <- B0
  strandings[1] <- round(q_buoy * phi * B0)
  # process model
  for (t in c(2:n_obs)) {
    g_prov <- log(strandings[t-1] + 
                    ((gamma_pt+1) * r_max / gamma_pt) * strandings[t-1] * (1 - (mu * strandings[t-1])^gamma_pt) - phi * strandings[t-1]
                  ) - 0.5 * sigma_env * sigma_env
    strandings[t] <- round(rtlnorm(1, location = g_prov, scale = sigma_env, left = 0))
    abundance[t] <- strandings[t] / (phi * q_buoy)	
  }
  ### observation model 
  obs_abundance <- round(rlnorm(length(abundance), log(abundance) -0.5 * tau * tau, tau))
  
  # Verification de l'existence de la log-vraisemblance optimale des strandings
  check_likelihood <- log_lik_strandings(y = strandings,
                                         q_buoy = q_buoy,
                                         phi = phi,
                                         r_max = r_max,
                                         theta0 = theta0,
                                         gamma_pt = gamma_pt,
                                         sigma_env = sigma_env,
                                         B0 = obs_abundance[1] # use the first observation here
                                         )
  if (is.infinite(check_likelihood) || is.na(check_likelihood)) {
    print("\t Probleme dans la generation des donnees")
  }
  
  out <- list("Ce" = strandings,
              "Bt" = round(abundance),
              "Bt.obs" = obs_abundance
              )
  
  return(out)
} 
