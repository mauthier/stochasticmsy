##--------------------------------------------------------------------------------------------------------
## SCRIPT : Données marsouins en mer du Nord
##
## Authors : Matthieu Authier & Fanny Ouzoulias
## Last update : 2022-02-15
##
## R version 3.6.3 (2020-02-29) -- "Holding the Windsock"
## Copyright (C) 2020 The R Foundation for Statistical Computing
## Platform: x86_64-pc-linux-gnu (64-bit)
##--------------------------------------------------------------------------------------------------------

lapply(c("tidyverse", "RLA"), 
       library, character.only = TRUE
       )

rm(list = ls())

data("north_sea_hp")

### un graphique blabla
data.frame(x = rnorm(1e4),
           y = rpois(1e4, lambda = 3)
           ) %>%
  ggplot(aes(x = x, y = y)) +
  geom_point() +
  theme_bw()
