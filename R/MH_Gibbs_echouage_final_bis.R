#### MH Gibbs échouages - loi beta - final

rm(list = ls())

################ Paramètres à choisir ######################

n <- 3 # nbre de chaines MCMC
K <- 5e5 # capacité biotique
r <- 0.4 # taux de croissance intrinsèque
n_data <- 30 # nb d'années de captures 
phi_value <- 0.05 # taux d'extraction
alpha <- 0.25 # pourcentage de K la première année pour la biomasse initiale (B0 = K * alpha)
sd_env <- 0.05 # stochasticité environnementale
sd_K <- 10 # écart type pour loi de proba de K
sd_r <- 0.04 # écart type pour loi de proba de r
q <- 0.8 # capturabilité 
M <- 1000 # itérations boucle MCMC
gamma <- 2.4 # paramètre SPM

source("R/functions/functions_echouages.R")

data <- x <- C <- round(spm(K, r, n_data, phi_value, alpha, sd_env, sd_K, sd_r, q)) # données de simulation

X <- matrix(rep(data,n), length(data), n) # matrice avec colonnes identiques (trois chaînes)

# Prior sur sur taux de croissance r
r_min <- 0.2 
r_max <- 1

# Prior sur taux d'extraction phi
phi_min <- 0
phi_max <- 0.25 

# Prior sur capacité biotique K
K_min <- 10000
K_max<- 1000000

# Prior sur sigma2
sigma2_min <- 0
sigma2_max <- 0.7 # 0.7 = valeur d'extinction 

####### Initialisations ###################

r <- runif(n, r_min, r_max)

phi <- runif (n, phi_min, phi_max)

K.log <-  runif(n, K_min.log, K_max.log)

sigma2 <- runif(n, sigma2_min, sigma2_max) 


##### outils pour la conservation des tirages #####
out.r <- matrix(r, ncol = n)
out.phi <- matrix(phi, ncol=n)
out.K <- matrix(K, ncol=n)
out.sigma2 <- matrix(sigma2, ncol=n)
accept <- matrix(NA, ncol = n)

layout(matrix(c(1:4),2,2))
layout.show(4)

############################ boucle ######################################
#c <- 0
k <- 0
while (k<M)
{
  k <- k+1
  
  #====================================================
  # Etape MH pour r
  
  # Simulation instrumentale
  r.tilde <- rinstr.r(n)
  
  # Calcul du log-rapport de metropolis
  print("MAJ de r")

  res1.r <- log.post(X, q, phi, r.tilde, K, gamma, sigma2) - log.post(X, q, phi, r, K, gamma, sigma2)
  res2.r <- log.dinstr.r(r.tilde) - log.dinstr.r(r)
  log.ratio.r <- res1.r + res2.r
  
  # MAJ------------------------------
  r.last <- r
  r <- r.tilde
  # Acceptation 
  U <- runif(n, 0, 1)
  test <- (log(U) <= log.ratio.r)
  # protection numérique
  test[is.na(test)]=0
  # Mise à jour (r = r.tilde avec proba log.ratio)
  r[!test] <- r.last[!test]
  
  #====================================================
  # Etape MH pour phi
  
  # Simulation instrumentale
  phi.tilde <- rinstr.phi(n)
  print("MAJ de phi")

  # Calcul du log-rapport de metropolis
  res1.phi <- log.post(X, q, phi.tilde, r, K, gamma, sigma2) - log.post(X, q, phi, r, K, gamma, sigma2)
  res2.phi <- log.dinstr.phi(phi.tilde) - log.dinstr.phi(phi)
  log.ratio.phi <- res1.phi + res2.phi
  
  # MAJ------------------------------
  phi.last <- phi
  phi <- phi.tilde
  # Acceptation 
  U <- runif(n, 0, 1)
  test <- (log(U) <= log.ratio.phi)
  # protection num?rique
  test[is.na(test)]=0
  # Mise à jour (mu = mu.tilde avec proba log.ratio)
  phi[!test] <- phi.last[!test]
  
  #====================================================
  # Etape MH pour K
  
  # Simulation instrumentale
  K.tilde <- rinstr.K(n)
  print("MAJ de K")
  
  # Calcul du log-rapport de metropolis
  res1.K <- log.post(X, q, phi, r, K.tilde, gamma, sigma2) - log.post(X, q, phi, r, K, gamma, sigma2)
  res2.K <- log.dinstr.K(K.tilde) - log.dinstr.K(K)
  log.ratio.K <- res1.K + res2.K
  
  # MAJ------------------------------
  K.last <- K
  K <- K.tilde
  # Acceptation 
  U <- runif(n, 0, 1)
  test <- (log(U) <= log.ratio.K)
  # protection num?rique
  test[is.na(test)]=0
  # Mise à jour 
  K[!test] <- K.last[!test]
  
  
  #====================================================
  # Etape MH pour sigma2
  
  # Simulation instrumentale
  sigma2.tilde <- rinstr.sigma2(n)
  print("MAJ de sigma2")
  # Calcul du log-rapport de metropolis
  res1.sigma2 <- log.post(X, q, phi, r, K, gamma, sigma2.tilde) - log.post(X, q, phi, r, K, gamma, sigma2)
  res2.sigma2 <- log.dinstr.sigma2(sigma2.tilde) - log.dinstr.sigma2(sigma2)
  log.ratio.sigma2 <- res1.sigma2 + res2.sigma2
  
  # MAJ------------------------------
  sigma2.last <- sigma2
  sigma2 <- sigma2.tilde
  # Acceptation 
  U <- runif(n, 0, 1)
  test <- (log(U) <= log.ratio.sigma2)
  # protection numerique
  test[is.na(test)]=0
  # Mise à jour 
  sigma2[!test] <- sigma2.last[!test]
  
  #a <- (1 - D(r, gamma, phi, q, K)*g(X, gamma, r, K, phi))/g(X, gamma, r, K, phi)
  #b <- (1/(1 - D(r, gamma, phi, q, K)*g(X, gamma, r, K, phi)))* ( (1/(D(r, gamma, phi, q, K)*g(X, gamma, r, K, phi))) - 2 + D(r, gamma, phi, q, K)*g(X, gamma, r, K, phi) )
  
  #if(sigma2 > a & sigma2 > b)
  #{
    
    #sigma2 <- sigma2.last
    #c <- c+1
  #}
  
  # Conservation des anciens tirages
  out.r <- rbind(out.r,
                 matrix(r, ncol = n)
  )
  
  out.phi <- rbind(out.phi,
                   matrix(phi, ncol = n)
  )
  
  out.K <- rbind(out.K,
                 matrix(K, ncol = n)
  )
  
  out.sigma2 <- rbind(out.sigma2,
                      matrix(sigma2, ncol = n)
  )
  
  matplot(x=0:k, y=out.r,type="l",xlab="nb iterations",ylab="r")
matplot(x=0:k, y=out.K,type="l",xlab="nb iterations",ylab="K")
matplot(x=0:k, y=out.phi,type="l",xlab="nb iterations",ylab="phi")
matplot(x=0:k, y=out.sigma2,type="l",xlab="nb iterations",ylab="sigma2")

  
}



#source("R/MH_Gibbs_echouage_final.R")







