##### Metropolis Hasting within Gibbs - données d'échouage

rm(list = ls())

################ Simulation des donnees #############################

n_data <- 30

B <- array(data = NA, dim = c(n_data))

gamma <- 2.4
phi_value <- 0.10 # identical each year
sd_K <- 10
sd_r <- 0.04
K <- 5e5
r <- 0.4
q <- 0.8
K <- rnorm(n_data, mean = K, sd = sd_K)
r <- rnorm(n_data, mean = r, sd = sd_r)

# environmental stochasticity 
sd_env = 0.05 

# Definition B for t=1 to n
B <- array(NA, dim = c(n_data))
C <- array(NA, dim = c(n_data))


  # Initial biomass and captures
  # biomass first year
  alpha <- 0.25 # purcentage of K the first year
  B[1] <- floor(alpha * K * exp(rnorm(1, mean = -0.5 * sd_env * sd_env, sd = sd_env)))
  # captures
  # Ct = phi * Bt
  phi <- array(data = phi_value, dim = c(n_data))  
  C[1] <- 25 
  
  # biomass simulation (lognormal random noise)
  for (t in 1:(n_data-1)) 
  {
    B_new <- max((B[t] + r * B[t] * (1 - ((B[t]^2.4)/K)) - phi * B[t]), 
                 floor(0.001 * K))
    eps_t <- - 0.5 * sd_env * sd_env + sd_env * rnorm(1) 
    log_Bnew <- log(B_new) + eps_t
    B[t + 1] <- floor(exp(log_Bnew))
    C[t + 1] <- B[t + 1] * phi_value * q
   
  }


  plot(1:n_data, C, type = "l", main = "Captures", ylim=c(0, 100))
  
  
  
  n <- 3 # nbre de chaines MCMC
  data <- x <- C
  
  X <- matrix(rep(data,n), length(data), n) # matrice avec colonnes identiques (trois chaînes)

  
############### Paramètres fixés ###################################

q <- 0.8 # capturabilité
gamma <- 2.4 # paramètre de l'équation 
M <- 1000 # itérations boucle MCMC
tau <- 1 # paramètre d'échelle random walk

################ Priors uniformes #############################################

# Prior sur sur taux de croissance r
r_min <- 0.2 
r_max <- 1

# Prior sur taux d'extraction phi
phi_min <- 0
phi_max <- 0.2 

# Prior sur capacité biotique K
K_min <- 10000
K_max <- 100000

# Prior sur sigma2
sigma2_min <- 0
sigma2_max <- 0.7 # 0.7 = valeur d'extinction 

######## Fonctions relatives au calcul du rapport Metropolis #############

################# r #####################

# Simulation instrumentale pour r : uniforme
rinstr.r <- function(n)
{
  runif(n, r_min , r_max)
}

# Log densité instrumentale sur r 
log.dinstr.r <- function(x)
{
  #res <- rep(log(1/(r_max - r_min)), length(x))
  res <- rep(-log((r_max - r_min)),length(x))
  return(res)
}

# Log densite loi a priori sur r
log.dens.prior.r <- function(x)
{
  #res <-rep(log(1/(r_max - r_min)), length(x))
  res <-rep(-log(r_max - r_min), length(x)) 
  return(res)
}


################ phi ###################

# Simulation instrumentale pour phi : uniforme
rinstr.phi <- function(n)
{
  runif(n, phi_min , phi_max)
}

# Log densité instrumentale sur phi
log.dinstr.phi <- function(x)
{
  res <- rep(log(1/(phi_max - phi_min)), length(x))
  return(res)
}

# Log densite loi a priori sur phi
log.dens.prior.phi <- function(x)
{
  res <- rep(-log(phi_max - phi_min), length(x))
  return(res)
}


############### K ######################

# Simulation instrumentale pour K : uniforme
rinstr.K <- function(n)
{
  runif(n, K_min , K_max)
}

# Log densité instrumentale sur K
log.dinstr.K <- function(x)
{
  res  <- rep(-log(K_max - K_min), length(x))
  return(res)
}

# Log densite loi a priori sur K
log.dens.prior.K <- function(x)
{
  res  <- rep(-log(K_max - K_min), length(x))
  return(res)
}


############## sigma2 ##################

# Simulation instrumentale pour sigma2 : uniforme
rinstr.sigma2 <- function(n)
{
  runif(n, sigma2_min , sigma2_max)
}

# Log densité instrumentale sur sigma2
log.dinstr.sigma2 <- function(x)
{
  res  <- rep(-log(sigma2_max - sigma2_min), length(x))
  return(res)
}

# Log densite loi a priori sur sigma2
log.dens.prior.sigma2 <- function(x)
{
  res  <- rep(-log(sigma2_max - sigma2_min), length(x))
  return(res)
}


########################## vraisemblance et posterior joint ####################################
g <- function(X, gamma, r, K, phi) {
  res <- X + r*X*((gamma +1)/gamma) * (1- (X/K)^gamma) - phi*X
  return(res)
}


D <- function(r, gamma, phi, q, K) { 
  res <- (r*(gamma + 1)/ (gamma - gamma*phi + r*(gamma + 1)))^(1/gamma) * 1/(q*phi*K)
  return(res)
}

pt <- function(X, sigma2, r, gamma, phi, K) {
  res <- 1/sigma2 * (1 - D(r, gamma, phi, q, K)*g(X, gamma, r, K, phi)) - D(r, gamma, phi, q, K)*g(X, gamma, r, K, phi)
  return(res)
}


qt <- function(sigma2, r, gamma, phi, q, K, X) { 
  res <- 1/sigma2 * ((1/D(r, gamma, phi, q, K)*g(X, gamma, r, K, phi)) - 2 + D(r, gamma, phi, q, K)*g(X, gamma, r, K, phi)) - 1 + D(r, gamma, phi, q, K)*g(X, gamma, r, K, phi)
  return(res)
}  


log.likelihood <- function(X, q, phi, r, K, gamma, sigma2) {
  X <- matrix(rep(data,n), length(data), n)
  res <- 0
  for (i in 2:n_data) {
    D <- ((r*(gamma + 1))/ (gamma - gamma*phi + r*(gamma + 1)))^(1/gamma) * 1/(q*phi*K)
    g <- X[i,] + r*X[i,]*((gamma +1)/gamma) * (1- (X[i,]/K)^gamma) - phi*X[i,]
    pt <- 1/sigma2 * (1 - D*g) - D*g
    qt <- 1/sigma2 * (1 - D*g) * (1/(D*g) - 1)
    res <- res + lgamma(pt+qt) - lgamma(pt) - lgamma(qt) + (pt - 1) * log(D*X[i,]) - (qt - 1) * log(1 - (D*X[i,])) - log(D) # production de NaN dans log(1 - (D*X[i]))
    }
    return(res)
}


log.post <- function(X, q, phi, r, K, gamma, sigma2)
{
  res <- log.likelihood(X, q, phi, r, K, gamma, sigma2) + log.dens.prior.r(r) + log.dens.prior.phi(phi) + log.dens.prior.K(K) + log.dens.prior.sigma2(sigma2)
  return(res)
}  


####### Initialisations ###################

r <- runif(n, r_min, r_max)

phi <- runif (n, phi_min, phi_max)

K <-  runif(n, K_min, K_max)

sigma2 <- runif(n, sigma2_min, sigma2_max) 


##### outils pour la conservation des tirages #####
out.r <- matrix(r, ncol = n)
out.phi <- matrix(phi, ncol=n)
out.K <- matrix(K, ncol=n)
out.sigma2 <- matrix(sigma2, ncol=n)
accept <- matrix(NA, ncol = n)
#mu.hist <- c()

############################ boucle ######################################

k <- 0
while (k<M)
{
  k <-k+1
  
  #====================================================
  # Etape MH pour r
  
  # Simulation instrumentale
  r.tilde <- rinstr.r(n)
  
  # Calcul du log-rapport de metropolis
  res1.r <- log.post(X, q, phi, r.tilde, K, gamma, sigma2) - log.post(X, q, phi, r, K, gamma, sigma2)
  res2.r <- log.dinstr.r(x) - log.dinstr.r(x)
  log.ratio.r <- res1.r + res2.r

  # MAJ------------------------------
  r.last <- r
  r <- r.tilde
  # Acceptation 
  U <- runif(n, 0, 1)
  test <- (log(U) <= log.ratio.r)
  # protection numérique
  test[is.na(test)]=0
  # Mise à jour (r = r.tilde avec proba log.ratio)
  r[!test] <- r.last[!test]
  
  #====================================================
  # Etape MH pour phi
  
  # Simulation instrumentale
  phi.tilde <- rinstr.phi(n)
  
  # Calcul du log-rapport de metropolis
  res1.phi <- log.post(X, q, phi.tilde, r, K, gamma, sigma2) - log.post(X, q, phi, r, K, gamma, sigma2)
  res2.phi <- log.dinstr.phi(x) - log.dinstr.phi(x)
  log.ratio.phi <- res1.phi + res2.phi
  
  # MAJ------------------------------
  phi.last <- phi
  phi <- phi.tilde
  # Acceptation 
  U <- runif(n, 0, 1)
  test <- (log(U) <= log.ratio.phi)
  # protection num?rique
  test[is.na(test)]=0
  # Mise à jour (mu = mu.tilde avec proba log.ratio)
  phi[!test] <- phi.last[!test]
  
  #====================================================
  # Etape MH pour K
  
  # Simulation instrumentale
  K.tilde <- rinstr.K(n)
  
  # Calcul du log-rapport de metropolis
  res1.K <- log.post(X, q, phi, r, K.tilde, gamma, sigma2) - log.post(X, q, phi, r, K, gamma, sigma2)
  res2.K <- log.dinstr.K(x) - log.dinstr.K(x)
  log.ratio.K <- res1.K + res2.K
  
  # MAJ------------------------------
  K.last <- K
  K <- K.tilde
  # Acceptation 
  U <- runif(n, 0, 1)
  test <- (log(U) <= log.ratio.K)
  # protection num?rique
  test[is.na(test)]=0
  # Mise à jour 
  K[!test] <- K.last[!test]
  
  
  #====================================================
  # Etape MH pour sigma2
  
  # Simulation instrumentale
  sigma2.tilde <- rinstr.sigma2(n)
  
  # Calcul du log-rapport de metropolis
  res1.sigma2 <- log.post(X, q, phi, r, K, gamma, sigma2.tilde) - log.post(X, q, phi, r, K, gamma, sigma2)
  res2.sigma2 <- log.dinstr.sigma2(x) - log.dinstr.sigma2(x)
  log.ratio.sigma2 <- res1.sigma2 + res2.sigma2
  
  # MAJ------------------------------
  sigma2.last <- sigma2
  sigma2 <- sigma2.tilde
  # Acceptation 
  U <- runif(n, 0, 1)
  test <- (log(U) <= log.ratio.sigma2)
  # protection num?rique
  test[is.na(test)]=0
  # Mise à jour 
  sigma2[!test] <- sigma2.last[!test]


  
  # Conservation des anciens tirages
  out.r <- rbind(out.r,
                  matrix(r, ncol = n)
  )
  
  out.phi <- rbind(out.phi,
                     matrix(phi, ncol = n)
  )
  
  out.K <- rbind(out.K,
                   matrix(K, ncol = n)
  )
  
  out.sigma2 <- rbind(out.sigma2,
                   matrix(sigma2, ncol = n)
  )
  
  #accept <- rbind(accept,test)
  
  
}


#layout(matrix(c(1:2),2,1))
#layout.show(2)

matplot(x=0:k, y=out.r,type="l",xlab="nb iterations",ylab="r")
#etc














