##--------------------------------------------------------------------------------------------------------
## SCRIPT : Modèle de simulation de biomasse
##
## Authors : Matthieu Authier & Fanny Ouzoulias
## Last update : 2022-02-18
##
## R version 3.6.3 (2020-02-29) -- "Holding the Windsock"
## Copyright (C) 2020 The R Foundation for Statistical Computing
## Platform: x86_64-pc-linux-gnu (64-bit)
##--------------------------------------------------------------------------------------------------------

lapply(c("tidyverse", "nimble"), 
       library, character.only = TRUE
       )

rm(list = ls())

# for reproducibility
set.seed(123)

# K: carrying capacity?
K <- 5e5

# growth rate?
r <- 0.4

# number of years of projections
n <- 22

# number of trajectories
n_traj <- 10

B <- array(data = NA, dim = c(n, n_traj))

# parameters Schaeffer
gamma <- 2.4
phi_value <- 0.10 # identical each year
sd_K <- 10
sd_r <- 0.04
K_traj <- rnorm(mean = K, sd = sd_K, n = n_traj)
r_traj <- rnorm(mean = r, sd = sd_r, n = n_traj)

# biomass first year
# B[1] = alpha*K
alpha <- 0.25 # purcentage of K the first year

# captures
# Ct = phi * Bt
# value for phi?
phi <- array(data = phi_value, dim = c(n, n_traj))

# environmental stochasticity 
sd_env = 0.05 

# Definition B for t=1 to n
B <- array(NA, dim = c(n, n_traj))
C <- array(NA, dim = c(n, n_traj))

# Loop on the trajectories
for (traj in 1:n_traj)
{
  
# Initial biomass 
B[1, traj] <- floor(alpha * K * exp(rnorm(1, 
                                    mean = -0.5 * sd_env * sd_env, 
                                    sd = sd_env
                                    )
                              ) 
              )
C[1, traj] <-  B[1, traj] * phi_value

# biomass simulation (lognormal random noise)
for (t in 1:(n-1)) 
{
  B_new <- max((B[t, traj] + r_traj[traj] * B[t, traj] * (1 - (B[t, traj]/K_traj[traj])^2.4 - phi * B[t, traj])), 
  floor(0.001 * K)               )

 log_Bnew <- rnorm(1, mean = log(B_new) - 0.5 * sd_env * sd_env, sd = sd_env)
 # same as
 # eps_t <- - 0.5 * sd_env * sd_env + sd_env * rnorm(1) # should we also store this eps_t value?
 # log_Bnew <- log(B_new) + eps_t
 B[t + 1, traj] <- floor(exp(log_Bnew))# round it to an integer for an abundance
 C[t + 1, traj] <- B[t + 1, traj] * phi_value
 
}; rm(t, log_Bnew)
}



# biomass trajectories
plot(1:n, B[,1], type = "l", main = "Biomass trajectories")

for (traj in 2:n_traj) {
  points(1:n, B[,traj], type = "l")
}


# function
schaeffer <- function(K, r, n, phi, alpha, sd_env, sd_K, sd_r, n_traj) 
  {
B <- array(data = NA, dim = c(n, n_traj))
phi <- array(data = phi, dim = c(n, n_traj))
gamma <- runif (0,5)
K_traj <- rnorm(mean = K, sd = sd_K, n = n_traj)
r_traj <- rnorm(mean = r, sd = sd_r, n = n_traj)
phi <- array(data = phi, dim = c(n, n_traj))
  
  for (traj in 1:n_traj)
  {
    B[1, traj] <- floor(alpha * K * exp(rnorm(1, 
                                              mean = -0.5 * sd_env * sd_env, 
                                              sd = sd_env
    )
    ) 
    )
    for (t in 1:(n-1)) 
    {
      B_new <- max((B[t, traj] + r_traj[traj] * B[t, traj] * (1 - (B[t, traj]/K_traj[traj])^2.4 - phi * B[t, traj])), 
                   floor(0.001 * K) # to make sure biomasses are positive
      )
      eps_t <- - 0.5 * sd_env * sd_env + sd_env * rnorm(1) 
      log_Bnew <- log(B_new) + eps_t
      B[t + 1, traj] <- floor(exp(log_Bnew)) 
    }; rm(t, log_Bnew)
    dataframe_B = as.data.frame(B)
  }
return(dataframe_B)
dataframe_B
}

# test function and plot
result <- schaeffer(5e5, 0.4, 23, 0, 0.25, 0.05, 10, 0.04, 10)

ggplot(result, aes(x=1:23, y=V1)) + geom_point() + geom_line(linetype = "dashed") 
# test for V1




