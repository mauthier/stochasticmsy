##--------------------------------------------------------------------------------------------------------
## SCRIPT : Results from simulation w 3D copula prior and a mis-specified model
##
## Authors : Matthieu Authier, Nicolas Bousquet & Fanny Ouzoulias
## Last update : 2022-08-04
##
## R version 4.0.5 (2021-03-31) -- "Shake and Throw"
## Copyright (C) 2021 The R Foundation for Statistical Computing
## Platform: x86_64-w64-mingw32/x64 (64-bit)
##--------------------------------------------------------------------------------------------------------

lapply(c("tidyverse", "rstan"), 
       library, character.only = TRUE
       )

rm(list = ls())

source("stan/20220801_tlnorm_stan.r")
### compiled model
spm <- stan_model(file = "stan/spm_3Dcopula.stan",
                  model_name = "Stochastic MSY with 3Dcopula prior"
                  )
spmgam <- stan_model(file = "stan/spm_3Dcopula_gamma.stan",
                     model_name = "Stochastic MSY with 3Dcopula prior and gamma marginal on r"
                     )
# load(file = "stan/StanModel3DCopula.RData")
# load(file = "stan/StanModel3DCopulaGamma.RData")

n_sim <- 100

### for reproducibility
seed_id <- c(20220621, 20220625, 20220628, 20220629, 20220602)
# 1 - 20220621
# 2 - 20220625 
# 3 - 20220628
# 4 - 20220629 
# 5 - 20220602

make_q <- function(n_obs, q = 0.5) {
  eps <- (1:n_obs - mean(1:n_obs)) / sd(1:n_obs) + rnorm(n_obs)
  return(plogis(log(q / (1- q)) + eps))
}
# cor(1:n_obs, make_q(n_obs = 30), method = "kendall")

### length of time series
n_obs <- 30 # nb d'années de captures 
### measurement error
cv <- 0.3
### carrying capacity
K <- 5e+05
D0 <- 0.9 # initial condition
r_max <- 0.04 # taux de croissance intrinsèque
phi <- 0.01 # taux d'extraction
gamma_pt <- 2.4 # paramètre SPM
assumed_q <- 1.0 #
sigma_env <- 0.10 # stochasticité environnementale
kendall_tau <- 0.3

for(i in 2:2){#length(seed_id)) {
  set.seed(seed_id[i])
  ### generate datasets
  all_simul_data <- replicate(n_sim,
                              pt_wrapper(q_buoy = make_q(n_obs = n_obs)),
                              simplify = FALSE
                              )
  
  # ### test Stan
  # system.time(
  #   test <- fitWstan(compiled_model = spm,
  #                    simul_data = all_simul_data[[1]],
  #                    survey_freq = 10,
  #                    copula = FALSE
  #                    )
  #   )
  # # utilisateur     système      écoulé
  # #       4.89        1.80       55.17
  # stan_rhat(test[[1]])
  # 
  # plot(test[[1]],
  #      plotfun = "stan_trace",
  #      pars = c("r", "phi", "sigma", "D0"),
  #      inc_warmup = TRUE
  #      )
  # 
  # print(test[[1]], digits = 4)
  
  ### fit all 
  # all_data_fit <- lapply(all_simul_data,
  #                        fitWstan,
  #                        compiled_model = spm,
  #                        survey_freq = 10,
  #                        copula = FALSE
  #                        )
  # 
  # save(list = c("all_data_fit", "all_simul_data"),
  #      file = paste("output/SimulSPMStan_Mismatch_Uni_r_", i, ".RData", sep = "")
  #      )
  # 
  # rm(all_data_fit)
  # gc()
  # 
  ### fit all with gamma prior
  # all_data_fit <- lapply(all_simul_data,
  #                        fitWstan,
  #                        compiled_model = spmgam,
  #                        survey_freq = 10,
  #                        copula = FALSE
  #                        )
  # 
  # save(list = c("all_data_fit", "all_simul_data"),
  #      file = paste("output/SimulSPMStan_Mismatch_Gam_r_", i, ".RData", sep = "")
  #      )
  # 
  # rm(all_data_fit)
  # gc()
  
  ### fit all with 3D copula prior
  all_data_fit <- lapply(all_simul_data,
                         fitWstan,
                         compiled_model = spm,
                         survey_freq = 10,
                         copula = TRUE
                         )
  
  save(list = c("all_data_fit", "all_simul_data"),
       file = paste("output/SimulSPMStan_Mismatch_Cop_r_", i, ".RData", sep = "")
       )
  
  rm(all_data_fit, all_simul_data)
  gc()
}
