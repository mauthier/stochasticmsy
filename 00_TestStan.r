##--------------------------------------------------------------------------------------------------------
## SCRIPT : Test Stan on Stochastic MSY model
##
## Authors : Matthieu Authier, Nicolas Bousquet & Fanny Ouzoulias
## Last update : 2022-06-21
##
## R version 4.0.5 (2021-03-31) -- "Shake and Throw"
## Copyright (C) 2021 The R Foundation for Statistical Computing
## Platform: x86_64-w64-mingw32/x64 (64-bit)
##--------------------------------------------------------------------------------------------------------

lapply(c("tidyverse", "lubridate", "rstan", "loo"), 
       library, character.only = TRUE
       )

### check inverse gamma prior
igam_shape <- 65
igam_scale <- 0.4 # be careful here between scale and rate parametrization
ir <- rgamma(1e6, shape = igam_shape, rate = 1 / igam_scale)
hist(1/ir)
summary(1/ir)
sqrt(var(1/ir)) / mean(1/ir) # 0.126
1 / (0.126 * 0.126)
1 / (0.04 * 0.126 * 0.126)

r <- rgamma(1e6, shape = 1 / (0.126 * 0.126), rate = 1 / (0.04 * 0.126 * 0.126))
hist(r)

rm(list = ls())


### simulation de données
source("R/functions/functions_echouages_07-06-2022.R")   # fonctions pour mettre en place le Gibbs

set.seed(20220622)
### length of time series
n_data <- 100 # nb d'années de captures 
### measurement error
cv <- 0.1
tau <- sqrt(log1p(cv * cv))
### carrying capacity
K <- 5e+05
theta0 <- 0.9 # initial condition
r <- 0.04 # taux de croissance intrinsèque
phi <- 0.005 # taux d'extraction
gamma <- 2.4 # paramètre SPM
q <- 1.0 # fraction échouages bycatch

alpha <- 1 # not used in code
sigma_square <- sqrt(0.01) # stochasticité environnementale

simul_data <- spm.data.generation(N = n_data,
                                  theta1 = theta0, 
                                  r = r, 
                                  phi = phi,
                                  sigma.square = sigma_square,
                                  gamma = gamma,
                                  q = q,
                                  alpha = alpha,
                                  cv.B.obs = cv, 
                                  B0.obs = rlnorm(1, 
                                                  meanlog = log(K * theta0) - 0.5 * tau * tau, 
                                                  sdlog = tau
                                                  ) %>%
                                    floor()  
                                  ) %>%
  do.call('cbind', .) %>%
  as.data.frame() %>%
  mutate(time = 1:n()) %>%
  pivot_longer(cols = -time,
               names_to = "param",
               values_to = "value"
               )

simul_data %>%
  ggplot(aes(x = time, y = value, group = param, color = param)) +
  geom_line() +
  scale_y_log10() +
  theme_bw()

### Stan
rstan_options(auto_write = TRUE)
options(mc.cores = parallel::detectCores())
spm <- stan_model(file = "stan/spm_unscaledprior.stan", 
                  model_name = "Stochastic MSY"
                  )

stan_data <- list(n_strandings = n_data,
                  n_surveys = length(c(1, seq(10, 100, 10))),
                  STRANDINGS = simul_data %>%
                    filter(param == "Ce") %>%
                    pull(value),
                  SURVEY = simul_data %>%
                    filter(time %in% c(1, seq(10, 100, 10))) %>%
                    pivot_wider(names_from = param,
                                values_from = value
                                ) %>%
                    select("Ce", "Bt.obs") %>%
                    mutate(cv = cv) %>%
                    as.matrix(),
                  gamma = gamma,
                  theta0 = theta0,
                  B0 = K * theta0,
                  q = 1.0,
                  upper_bound_r = 0.1,
                  upper_bound_phi = 0.1
                )

### test
fit = sampling(spm, 
               data = stan_data, 
               iter = 1000, 
               warmup = 500, 
               thin = 1, 
               chains = 4, 
               control = list(max_treedepth = 15, adapt_delta = 0.95),
               pars = c("r", "phi", "sigma")
               )


stan_rhat(fit)

plot(fit,
     plotfun = "stan_trace",
     pars = c("r", "phi", "sigma"),
     inc_warmup = TRUE
     )

print(fit2, digits = 4)

plot(fit,
     plotfun = "stan_hist",
     pars = c("r", "phi", "sigma"),
     inc_warmup = FALSE
     )

plot(fit,
     plotfun = "stan_dens",
     pars = c("r", "phi", "sigma"),
     inc_warmup = FALSE
     )

pairs(fit)

### try a Gumbel copula just for fun
spm_gumbel <- stan_model(file = "stan/spm_unscaledprior_gumbelcopula.stan", 
                  model_name = "Stochastic MSY"
                  )

stan_data$kendalls_tau <- 0.7
fit2 = sampling(spm_gumbel, 
                data = stan_data, 
                iter = 1000, 
                warmup = 500, 
                thin = 1, 
                chains = 4, 
                control = list(max_treedepth = 15, adapt_delta = 0.95),
                pars = c("r", "phi", "sigma")
                )

stan_rhat(fit2)

plot(fit2,
     plotfun = "stan_trace",
     pars = c("r", "phi", "sigma"),
     inc_warmup = TRUE
     )

print(fit2, digits = 4)

### try an informative inverse gamma prior on r --> pb on prior here
### try an informative gamma prior on r
spm_gamma <- stan_model(file = "stan/spm_gam.stan", 
                        model_name = "Stochastic MSY"
                        )

fit3 = sampling(spm_gamma, 
                data = list(n_strandings = n_data,
                            n_surveys = length(c(1, seq(10, 100, 10))),
                            STRANDINGS = simul_data %>%
                              filter(param == "Ce") %>%
                              pull(value),
                            SURVEY = simul_data %>%
                              filter(time %in% c(1, seq(10, 100, 10))) %>%
                              pivot_wider(names_from = param,
                                          values_from = value
                                          ) %>%
                              select("Ce", "Bt.obs") %>%
                              mutate(cv = cv) %>%
                              as.matrix(),
                            gamma = gamma,
                            theta0 = theta0,
                            B0 = K * theta0,
                            q = 1.0,
                            upper_bound_phi = 0.1
                            ), 
                iter = 1000, 
                warmup = 500, 
                thin = 1, 
                chains = 4, 
                control = list(max_treedepth = 15, adapt_delta = 0.95),
                pars = c("r", "phi", "sigma", "check")
                )

stan_rhat(fit3)

plot(fit3,
     plotfun = "stan_trace",
     pars = c("r", "phi", "sigma", "check"),
     inc_warmup = TRUE
     )

print(fit3, digits = 4)
