##--------------------------------------------------------------------------------------------------------
## SCRIPT : Copula
##
## Authors : Matthieu Authier, Nicolas Bousquet, Fanny Ouzoulias
## Last update : 2022-08-01
##
## R version 4.0.5 (2021-03-31) -- "Shake and Throw"
## Copyright (C) 2021 The R Foundation for Statistical Computing
## Platform: x86_64-w64-mingw32/x64 (64-bit)
##--------------------------------------------------------------------------------------------------------

lapply(c("tidyverse", "rstan"), 
       library, character.only = TRUE
       )

### corrected for buoyancy
# q <- 1
# stranding <- read.table(file = paste("data", "Dd_mort_month_1990-2020.txt", sep = "/"),
#                         header = TRUE, dec = ",",
#                         colClasses = "numeric"
#                         ) %>%
#   rename(estimate = est_mean,
#          lower = est_min,
#          upper = est_max
#          ) %>%
#   group_by(year) %>%
#   summarize(estimate = round(sum(estimate)))

### not corrected for buoyancy
q <- 0.24
stranding <- read.table(file = paste("data", "mortality_1990-2021_bef_float.txt", sep = "/"),
                        header = TRUE, dec = ",",
                        colClasses = "numeric"
                        ) %>%
  rename(estimate = est)

with(stranding, cor(year, estimate, method = "kendall"))

stranding %>%
  ggplot(aes(x = year, y = estimate / q)) +
  geom_line() +
  scale_y_log10(name = "Bycatch estimate\n from strandings (log scale)") +
  scale_x_continuous(name = "Year", breaks = seq(1990, 2020, 2)) +
  theme_bw()

### abundance estimates
## common dolphins only
scans <- data.frame(year = c(2005, 2016),
                    abundance = c(468356, 487094),
                    cv = c(0.333, 0.255)
                    ) %>%
## common dolphins + striped dolphins + unidentified
# scans <- data.frame(year = c(2005, 2016),
#                     abundance = c(764482, 1031395),
#                     cv = c(0.345, 0.175)
#              ) %>%
  left_join(.,
            stranding,
            by = "year"
            ) %>%
  select(year, estimate, abundance, cv)

### Stan
source("stan/20220624_tlnorm_stan.R")
rstan_options(auto_write = TRUE)
options(mc.cores = parallel::detectCores())
# uniform prior on r
spm <- stan_model(file = "stan/spm_3Dcopula.stan",
                  model_name = "Stochastic MSY"
                  )
# # Gamma prior on r
# spm <- stan_model(file = "stan/spm_3Dcopula_gamma.stan", 
#                   model_name = "Stochastic MSY"
#                   )
# # Inverse Gamma prior on r
# spm <- stan_model(file = "stan/spm_3Dcopula_invgamma.stan", 
#                   model_name = "Stochastic MSY"
#                   )
### format the data
## ignore the copula her by setting COPULA = 0
data4stan <- list(n_strandings = nrow(stranding),
                  n_surveys = nrow(scans),
                  STRANDINGS = stranding %>%
                    pull(estimate),
                  SURVEY = scans %>%
                    select("estimate", "abundance", "cv") %>%
                    as.matrix(),
                  gamma = 2.4,
                  B0 = scans %>%
                    pull(abundance) %>%
                    first(),
                  q = q,
                  upper_bound_phi = 0.1,
                  upper_bound_r = 0.1,
                  upper_bound4sigma = 2.0,
                  lower_bound_D0 = 0.1,
                  theta_RG = -1.21,
                  theta_G = 0.39,
                  theta_RT = c(2.97, 0.18),
                  COPULA = 0
                  )

### sampling
fit = sampling(spm, 
               data = data4stan, 
               iter = 5000, 
               warmup = 1000, 
               thin = 4, 
               chains = 4, 
               control = list(max_treedepth = 15, adapt_delta = 0.99),
               pars = c("r", "phi", "sigma", "D0", "K")
               )

### convergence assessment
stan_rhat(fit)

### trace
plot(fit,
     plotfun = "stan_trace",
     pars = c("r", "phi", "sigma", "D0", "K"),
     inc_warmup = TRUE
     )

### estimates
print(fit, digits = 4)

### posteriors
plot(fit,
     plotfun = "stan_hist",
     pars = c("r", "phi", "sigma", "D0", "K"),
     inc_warmup = FALSE
     )

plot(fit,
     plotfun = "stan_dens",
     pars = c("r", "phi", "sigma", "D0", "K"),
     inc_warmup = FALSE
     )

pairs(fit)

scans %>%
  pull(abundance) %>%
  last() * rstan::extract(fit, "phi")$phi %>%
  summary()

### Sue now the copula prior
data4stan$COPULA = 1

### sampling
fit2 = sampling(spm, 
                data = data4stan, 
                iter = 5000, 
                warmup = 1000, 
                thin = 4, 
                chains = 4, 
                control = list(max_treedepth = 15, adapt_delta = 0.99),
                pars = c("r", "phi", "sigma", "D0", "K")
                )

### convergence assessment
stan_rhat(fit2)

### trace
plot(fit2,
     plotfun = "stan_trace",
     pars = c("r", "phi", "sigma", "D0", "K"),
     inc_warmup = TRUE
     )

### estimates
print(fit2, digits = 4)

### posteriors
plot(fit2,
     plotfun = "stan_hist",
     pars = c("r", "phi", "sigma", "D0", "K"),
     inc_warmup = FALSE
     )

plot(fit2,
     plotfun = "stan_dens",
     pars = c("r", "phi", "sigma", "D0", "K"),
     inc_warmup = FALSE
     )

pairs(fit2)

scans %>%
  pull(abundance) %>%
  last() * rstan::extract(fit2, "phi")$phi %>%
  summary()
