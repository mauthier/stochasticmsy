##--------------------------------------------------------------------------------------------------------
## SCRIPT : Variable q
##
## Authors : Matthieu Authier, Nicolas Bousquet & Fanny Ouzoulias
## Last update : 2022-08-03
##
## R version 4.0.5 (2021-03-31) -- "Shake and Throw"
## Copyright (C) 2021 The R Foundation for Statistical Computing
## Platform: x86_64-w64-mingw32/x64 (64-bit)
##--------------------------------------------------------------------------------------------------------

lapply(c("tidyverse", "rstan"), 
       library, character.only = TRUE
       )

rm(list = ls())

source("stan/20220801_tlnorm_stan.r")
### compiled model
spm <- stan_model(file = "stan/spm_3Dcopula.stan",
                  model_name = "Stochastic MSY with 3Dcopula prior"
                  )
### hyperparameters for the simulation
### length of time series
n_obs <- 100 # nb d'années de captures 
### measurement error
cv <- 0.3
### carrying capacity
K <- 5e+5
D0 <- 0.9 # initial condition
r_max <- 0.04 # taux de croissance intrinsèque
phi <- 0.01 # taux d'extraction
gamma_pt <- 2.4 # paramètre SPM
sigma_env <- 0.05 # stochasticité environnementale
kendall_tau <- NULL # no monotonicity constraint

### simulate a data set with a single q
q_buoy <- 0.25 # fraction échouages bycatch

### for reproducibility
set.seed(20220803)

sim_data_1 <- spm_wrapper()
sim_data_1 %>%
  ggplot(aes(x = time, y = value)) +
  geom_line() +
  facet_wrap(~param, scales = "free_y") +
  theme_bw()

stan_data_1 <- stan_data(simul_data = sim_data_1, 
                         survey_time = c(1, seq(10, 100, 1)), 
                         copula = FALSE # do not use the copula prior
                         )
stan_data_1$q <- q_buoy # pass the correct q

fit_1 <- sampling(spm, 
                  data = stan_data_1, 
                  iter = 1000, 
                  warmup = 500, 
                  thin = 1, 
                  chains = 4, 
                  control = list(max_treedepth = 15, adapt_delta = 0.95),
                  pars = c("r", "phi", "sigma", "D0", "K")
                  )

print(fit_1, digits = 3)

### simulate a data set with a varying q
q_buoy <- rbeta(n_obs, 71, 214) # fraction échouages bycatch entre 0.2 et 0.3

### for reproducibility
set.seed(20220803)

sim_data_2 <- spm_wrapper()
sim_data_2 %>%
  ggplot(aes(x = time, y = value)) +
  geom_line() +
  facet_wrap(~param, scales = "free_y") +
  theme_bw()

stan_data_2 <- stan_data(simul_data = sim_data_2, 
                         survey_time = c(1, seq(10, 100, 1)), 
                         copula = FALSE # do not use the copula prior
                         )
stan_data_2$q <- mean(q_buoy) # suppose you only have one value for q

fit_2 <- sampling(spm, 
                  data = stan_data_2, 
                  iter = 1000, 
                  warmup = 500, 
                  thin = 1, 
                  chains = 4, 
                  control = list(max_treedepth = 15, adapt_delta = 0.95),
                  pars = c("r", "phi", "sigma", "D0", "K")
                  )

print(fit_2, digits = 3)
### estimation of sigma seems ok here


### if q is not of length 1 or n_obs, error at the data simulation step
q_buoy <- rbeta(n_obs - 1, 71, 214) # fraction échouages bycatch entre 0.2 et 0.3

### for reproducibility
set.seed(20220803)

sim_data_3 <- spm_wrapper()